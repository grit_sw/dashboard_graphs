
export const DATA_FETCH: string = 'DATA_FETCH';
export const DATA_FETCHING: string = 'DATA_FETCHING';
export const DATA_FETCH_ERROR: string = 'DATA_FETCH_ERROR';
export const DATA_ADD: string = 'DATA_ADD';
