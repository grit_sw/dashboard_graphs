export const DAILY_DATA_FETCH: string = 'DAILY_DATA_FETCH';
export const HOURLY_DATA_FETCH: string = 'HOURLY_DATA_FETCH';
export const DAILY_DATA_FETCHING: string = 'DAILY_DATA_FETCHING';
export const HOURLY_DATA_FETCHING: string = 'HOURLY_DATA_FETCHING';
export const DAILY_DATA_FETCH_ERROR: string = 'DAILY_DATA_FETCH_ERROR';
export const HOURLY_DATA_FETCH_ERROR: string = 'HOURLY_DATA_FETCH_ERROR';
export const DAILY_DATA_ADD: string = 'DAILY_DATA_ADD';
export const HOURLY_DATA_ADD: string = 'HOURLY_DATA_ADD';
