import React, { Component } from 'react';
import Sidebar from '../components/GraphControls/Sidebar';
import Graphs from './Graphs';
import Login from '../components/GraphControls/Login';
import Export from '../components/GraphControls/Export';
import ExportContainer from '../components/GraphControls/ExportContainer';
import FilterContainer from '../components/GraphControls/FilterContainer';
import Container from '@material-ui/core/Container';
import Logout from '../components/GraphControls/Logout';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import FilterIcon from '@material-ui/icons/FilterList';
import { getAuth } from '../selectors/auth';
import { connect } from 'react-redux';
//For testing graph colors
import { doFetchConfig } from "../actions/global";
import { doSaveUserDetails } from "../actions/user";

interface HomeProps {
    auth: boolean;
    onFetchConfig: any;
    onFetchUser: any;
    globalState: any
}

class Home extends Component<HomeProps>{
    constructor(props: HomeProps) {
        super(props)
    }

    componentDidMount() {
        if (!localStorage.getItem('token')) {
            this.props.onFetchConfig('demo');
            this.props.onFetchUser({
                name: 'Demo'
            })
        } else {
            this.props.onFetchConfig('me');
            this.props.onFetchUser({
                name: localStorage.getItem('full_name')
            })
        }
    }

    render() {
        const Routes = [
            { name: 'Export', path: '/export', component: ExportContainer, iconComp: InboxIcon, open: false },
            { name: 'Filter', path: '/filter', component: FilterContainer, iconComp: FilterIcon, open: false },
        ];

        let authRoute: any;
        //   if (localStorage.getItem('token')) {
        //     this.props.onUpdateAuth(true)
        //   }
        //   if (this.props.auth) {
        if (localStorage.getItem('token')) {
            authRoute = { name: 'Logout', path: '/logout', component: Logout, iconComp: InboxIcon, open: false }

        } else {
            authRoute = { name: 'Login', path: '/login', component: Login, iconComp: InboxIcon, open: false }
        }

        Routes.push(authRoute);
        console.log(Routes);
        console.log('it works')

        return (
            <Container>
                <Sidebar SidebarRoutes={Routes} />
                <Graphs />
            </Container>
        );
    }
}


const mapStateToProps = (state: any) => ({
    auth: getAuth(state)
})

const mapDispatchToProps = (dispatch: any) => ({
    onFetchConfig: (type: string) => {
        dispatch(doFetchConfig(type))
    },
    onFetchUser: (userObj: any) => {
        dispatch(doSaveUserDetails(userObj))
    }
})



export default connect(mapStateToProps, mapDispatchToProps)(Home);
