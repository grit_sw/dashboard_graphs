import React, { Component } from 'react';
import { withStyles } from '@material-ui/core';
import Moment from 'moment';
import { connect } from 'react-redux';
import Power from '../components/Power';
import Power2 from '../components/Power2';
import Energy from '../components/Energy';
import Summary from '../components/Summary';
import Quality from '../components/Quality';
import Fuel from '../components/Fuel';
import Batteries from '../components/Batteries';
import { doFetchDailyData } from '../actions/daily';
import { doFetchHourlyData } from '../actions/hourly';
import { doSaveConfig } from '../actions/global';
import { doFetchAnalyticsData } from '../actions/data';
import { doFetchMonthlyData } from '../actions/monthly';
import { doFetchRecentData } from '../actions/recent';
import { getConfig } from '../selectors/global';
import store from '../store';

const styles = () => ({
	container: {
		display: 'flex',
		flexWrap: "wrap" as "wrap", 
		flexDirection: 'column' as "column",
		justifyContent: 'center',
		alignItems: 'center',
		width: '65vw',
		margin: '5vw',
	},
	content: {
		width: 'inherit',
	},
})

interface configObject {
	id: string,
	name: string
}

interface IQualityProps {
	classes: any; // added by material-ui withStyles css
	// dailyData: any;
	config: configObject,
    onFetchDailyData: any;
    onFetchHourlyData: any;
	onFetchMonthlyData: any;
	onFetchAnalyticsData: any;
	onFetchRecentData: any;
	saveConfigID: any;
}

interface IQualityValues {
	// configID: string,
	lastRequest: string,
	RecentDataInterval: string,
	timerID: any,
	initialLoadComplete: boolean,
}

class Graphs extends Component<IQualityProps, IQualityValues> {
	constructor(props: IQualityProps) {
		super(props)
		this.state = {
            // configID: 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF',
			// configID: 'HOI2M31GEUP1JSFPAY3VUHUL43GXMEUJ',
			lastRequest: '2019-05-01T18:35:00.000+01:00',
			RecentDataInterval: '300',
			timerID: 0,
			initialLoadComplete: false
		}
	}

	componentDidMount() {
        console.log(getConfig)
		// this.props.saveConfigID(this.props.configID)
        this.Daily()
        this.Hourly()
		this.Monthly()
		this.Data()
		this.Recent()
		let timerID = setInterval(
			() => this.RealTime(),
			10000
		);
        this.setState({timerID});
	}

	componentWillUnmount() {
		clearInterval(this.state.timerID);
	}

	RealTime() {
		if (this.props.config.id) {
        this.Daily()
        this.Hourly()
		this.Monthly()
		this.Data()
		this.Recent()
		}
	}

	Recent = () => {
		// This is used to draw the current, voltage and power factor graphs
		let params = {
			configId: this.props.config.id,
			timeStart: Moment().format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
			// timeStart: this.state.lastRequest,
			secondsInterval: this.state.RecentDataInterval,
		}
		console.log(params)
		this.props.onFetchRecentData(params)
	}

	Daily = () => {
		// Used to draw the Power AreaSeries Graph
		let params = {
			configId: this.props.config.id,
			timeStart: Moment().startOf('day').format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
			timeStop: Moment().endOf('day').format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
			// timeStart: '2019-05-01T00:00:00.000+01:00',
			// timeStop: '2019-05-01T23:59:00.000+01:00',
		}
		this.props.onFetchDailyData(params)
    }
    
    Hourly = () => {
		// Used to draw the Power AreaSeries Graph
		let params = {
			configId: this.props.config.id,
			timeStart: Moment().startOf('hour').format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
			timeStop: Moment().endOf('hour').format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
			// timeStart: '2019-05-01T00:00:00.000+01:00',
			// timeStop: '2019-05-01T23:59:00.000+01:00',
        }
		this.props.onFetchHourlyData(params)
	}

	Data = () => {
		// This method draws the four PieDonut Summary charts,
		// the fuel Line charts and the Batteries charts
		let params = {
			configId: this.props.config.id,
			timeStart: Moment().startOf('day').format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
			timeStop: Moment().endOf('day').format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
		}
		this.props.onFetchAnalyticsData(params);
	}

	Monthly = () => {
		// Used to draw the Energy BarSeries Graph
		let params = {
			configId: this.props.config.id,
			timeStart: Moment().startOf('month').format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
			timeStop: Moment().endOf('month').format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
		}
		this.props.onFetchMonthlyData(params)
	}

	render() {
		const { classes } = this.props;
		return (
			<div className={classes.container}>
				G1 Energy and Power Graphs
				<div className={classes.content}>
					<Power />
				</div>
                <div className={classes.content} style={{display: 'none'}}>
					<Power2 />
				</div>
				<div className={classes.content}>
					<Energy />
				</div>
				<div>
					<Summary />
				</div>
				<div>
					<Quality />
				</div>
				<div>
					<Fuel />
				</div>
				<div>
					<Batteries />
				</div>
			</div>
		);
	}
}


const mapDispatchToProps = (dispatch: any) => ({
    onFetchDailyData: (query: any)  => {
		!!query.configId && dispatch(doFetchDailyData(query))
    },
    onFetchHourlyData: (query: any)  => {
		!!query.configId && dispatch(doFetchHourlyData(query))
	},
    onFetchMonthlyData: (query: any)  => {
		!!query.configId && dispatch(doFetchMonthlyData(query))
	},
    onFetchRecentData: (query: any)  => {
		!!query.configId && dispatch(doFetchRecentData(query))
	},
    onFetchAnalyticsData: (query: any)  => {
		!!query.configId && dispatch(doFetchAnalyticsData(query))
	},
	saveConfigID: (configID: string) => {
		dispatch(doSaveConfig(configID))
	}
});

const mapStateToProps = (state : any) => ({
	config: getConfig(state)
})

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, { withTheme: true })(Graphs));
