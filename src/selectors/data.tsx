const getAnalyticsData = (state: any) => {
    // console.log(state.analyticsState)
    return state.analyticsState;
}

const getAnalyticsDataError = (state: any) => {
    return state.analyticsState ? state.analyticsState.error : false;
}

export {
    getAnalyticsData,
    getAnalyticsDataError,
};
