const getDailyData = (state: any) => {
    // console.log(state.dailyDataState)
    return state.dailyDataState;
}

const getDailyDataError = (state: any) => {
    return state.dailyDataState ? state.dailyDataState.error : false;
}

const getHourlyData = (state: any) => {
    // console.log(state.dailyDataState)
    return state.hourlyDataState;
}

const getHourlyDataError = (state: any) => {
    return state.hourlyDataState ? state.hourlyDataState.error : false;
}

export {
    getDailyData,
    getDailyDataError,
    getHourlyData,
    getHourlyDataError
};
