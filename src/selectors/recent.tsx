const getRecentData = (state: any) => {
    // console.log(state.recentDataState)
    return state.recentDataState;
}

const getRecentDataError = (state: any) => {
    return state.recentDataState ? state.recentDataState.error : false;
}

export {
    getRecentData,
    getRecentDataError,
};
