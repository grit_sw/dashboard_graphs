const getMonthlyData = (state: any) => {
    // console.log(state.monthlyDataState)
    return state.monthlyDataState;
}

const getMonthlyDataError = (state: any) => {
    return state.monthlyDataState ? state.monthlyDataState.error : false;
}

export {
    getMonthlyData,
    getMonthlyDataError,
};
