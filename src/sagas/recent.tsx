import { call, put } from 'redux-saga/effects';
import { doAddRecentData, doFetchErrorRecentData } from '../actions/recent';
import GetRecent from '../api/recent';

interface IAction {
    configId: string,
    timeStart: string,
    secondsInterval: string
}

interface IRecentAction {
    query: IAction,
    type: string,
}

function* handleFetchRecent(recentAction: IRecentAction) {
    const { configId, timeStart, secondsInterval } = recentAction.query;

	try {
        const result = yield call(GetRecent, configId, timeStart, secondsInterval);
		yield put(doAddRecentData(result));
	} catch (error) {
		yield put(doFetchErrorRecentData(error));
	}
}

export {
    handleFetchRecent,
};
