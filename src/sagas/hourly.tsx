import { call, put } from 'redux-saga/effects';
import { doAddHourlyData, doFetchErrorHourlyData } from '../actions/hourly';
import GetHourly from '../api/hourly';

interface IAction {
    configId: string,
    timeStart: string,
    timeStop: string
}

interface IHourlyAction {
    query: IAction,
    type: string,
}

function* handleFetchHourly(hourlyAction: IHourlyAction) {
    const { configId, timeStart, timeStop } = hourlyAction.query;

	try {
        const result = yield call(GetHourly, configId, timeStart, timeStop);
		yield put(doAddHourlyData(result));
	} catch (error) {
		yield put(doFetchErrorHourlyData(error));
	}
}

export {
    handleFetchHourly,
};
