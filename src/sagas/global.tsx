import { call, put } from 'redux-saga/effects';
import { doSaveConfig } from '../actions/global';
import { doSaveError } from '../actions/error';
import userConfig  from '../api/config';

function* handleFetchConfig(action: any) {
    const { urlType } = action;
    try {

        const data = yield call(userConfig, urlType);
        console.log(data)
        let {data: {data: configData}} = data
        console.log(configData);
        let selectedConfig
        if(Array.isArray(configData)) {
            selectedConfig = {
                name :configData[0].configuration_name,
                id: configData[0].configuration_id
            }
        } else {
            selectedConfig = {
                name :configData.configuration_name,
                id: configData.configuration_id
            } 
        }
        yield put(doSaveConfig(selectedConfig))
        yield put(doSaveError(null))
    } catch(error) {
        console.log(error.message)
        yield put(doSaveError(error.message))
    }
}

export {
    handleFetchConfig
}
