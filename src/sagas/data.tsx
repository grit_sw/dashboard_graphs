import { call, put } from 'redux-saga/effects';
import { doAddAnalyticsData, doFetchErrorAnalyticsData } from '../actions/data';
import GetAnalytics from '../api/data';

interface IAction {
    configId: string,
    timeStart: string,
    timeStop: string
}

interface IAnalyticsAction {
    query: IAction,
    type: string,
}

function* handleFetchAnalytics(dailyAction: IAnalyticsAction) {
    const { configId, timeStart, timeStop } = dailyAction.query;

	try {
        const result = yield call(GetAnalytics, configId, timeStart, timeStop);
		yield put(doAddAnalyticsData(result));
	} catch (error) {
		yield put(doFetchErrorAnalyticsData(error));
	}
}

export {
    handleFetchAnalytics,
};
