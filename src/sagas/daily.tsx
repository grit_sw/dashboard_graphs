import { call, put } from 'redux-saga/effects';
import { doAddDailyData, doFetchErrorDailyData } from '../actions/daily';
import GetDaily from '../api/daily';

interface IAction {
    configId: string,
    timeStart: string,
    timeStop: string
}

interface IDailyAction {
    query: IAction,
    type: string,
}

function* handleFetchDaily(dailyAction: IDailyAction) {
    const { configId, timeStart, timeStop } = dailyAction.query;

	try {
        const result = yield call(GetDaily, configId, timeStart, timeStop);
		yield put(doAddDailyData(result));
	} catch (error) {
		yield put(doFetchErrorDailyData(error));
	}
}

export {
    handleFetchDaily,
};
