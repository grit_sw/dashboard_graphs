import { call, put } from 'redux-saga/effects';
import { doAddMonthlyData, doFetchErrorMonthlyData } from '../actions/monthly';
import GetMonthly from '../api/monthly';

interface IAction {
    configId: string,
    timeStart: string,
    timeStop: string
}

interface IMonthlyAction {
    query: IAction,
    type: string,
}

function* handleFetchMonthly(monthlyAction: IMonthlyAction) {
    const { configId, timeStart, timeStop } = monthlyAction.query;

	try {
        const result = yield call(GetMonthly, configId, timeStart, timeStop);
		yield put(doAddMonthlyData(result));
	} catch (error) {
		yield put(doFetchErrorMonthlyData(error));
	}
}

export {
    handleFetchMonthly,
};
