import { takeEvery, all } from 'redux-saga/effects';
import { DAILY_DATA_FETCH } from '../constants/dailyActionTypes';
import { HOURLY_DATA_FETCH } from '../constants/dailyActionTypes';
import { DATA_FETCH } from '../constants/dataActionTypes';
import { handleFetchDaily } from './daily';
import { handleFetchHourly } from './hourly';
import { handleFetchAnalytics } from './data';
import { MONTHLY_DATA_FETCH } from '../constants/monthlyActionTypes';
import { handleFetchMonthly } from './monthly';
import { RECENT_DATA_FETCH } from '../constants/recentActionTypes';
import { handleFetchRecent } from './recent';
import { FETCH_CONFIG } from '../constants/globalActionTypes';
import { handleFetchConfig} from './global';

function *watchAll() {
	yield all([
        takeEvery(DAILY_DATA_FETCH, handleFetchDaily),
        takeEvery(HOURLY_DATA_FETCH, handleFetchHourly),
		takeEvery(MONTHLY_DATA_FETCH, handleFetchMonthly),
		takeEvery(RECENT_DATA_FETCH, handleFetchRecent),
		takeEvery(DATA_FETCH, handleFetchAnalytics),
		takeEvery(FETCH_CONFIG, handleFetchConfig),
	])
}

export default watchAll;
