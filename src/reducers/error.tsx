import { ADD_ERROR } from '../constants/errorActionType';

const INITIAL_STATE = {
	error: null
};
  
const applySaveError = (state: any, action: any) => ({
   error: action.error 
})
  
function errorReducer(state = INITIAL_STATE, action: any) {
	switch(action.type) {
		case ADD_ERROR : {
			return applySaveError(state, action);
		}
		default : return state;
	}
}

export default errorReducer;