import { CHANGE_AUTH } from '../constants/authActionType'

const INITIAL_STATE = {
    auth: false
}

const applyChangeAuth = (state: any, action: any) => {
    return Object.assign({}, state, {
        auth : action.auth
    })
}

function authReducer(state = INITIAL_STATE, action: any) {
    switch(action.type) {
        case CHANGE_AUTH : {
            return applyChangeAuth(state, action)
        }
        default : return state
    }
}

export default authReducer;