import { DAILY_DATA_ADD, DAILY_DATA_FETCH_ERROR, DAILY_DATA_FETCHING } from '../constants/dailyActionTypes';
import Moment from 'moment';

const INITIAL_STATE = {
	DailyGridPower: [],
	DailyGeneratorPower: [],
	DailyInverterPower: [],
	DailySolarPower: [],
	error: false,
	OpComplete: false,
};
  
const applyAddDailyData = (state: any, action: any) => {
	let configId = action.configID;
	let DataDaily = [];
	let dataDaily: Array<any> = [];
	let plotdata_daily = action.res.data.aggregations.Power_.histogram.buckets;
	plotdata_daily.forEach((d: any) => {
		d.src = [];
	});
	plotdata_daily.forEach((d: any) =>{
		d.buckets = [];
		d.powerNest.configID.buckets.forEach((c: any) =>{
			if (c.key.toLowerCase() === configId.toLowerCase()){
			d.buckets = d.powerNest.SourceType.buckets;
			} else {
				d.buckets = [];
			}
		});
	});
	plotdata_daily.forEach((d: any) => {
		let activeSources = d.activeSource.sourceType.buckets.map((bucket: any) =>{
			return bucket.key;
		});
		for (let i = 0; i < d.buckets.length; i++) {
			if(activeSources.indexOf(d.buckets[i].key) != -1){
				d.src.push({
					"key": d.buckets[i].key,
					"value": d.powerNest.power.avg
				});
			}
		}
	});

	plotdata_daily.forEach((d: any) => {
		for (let i = 0; i < d.buckets.length; i++) {
			DataDaily.push({
				"key": d.buckets[i].key,
				"value": d.powerNest.power.avg || 0,
				"time": Moment(d.key_as_string).format("YYYY-MM-DDTHH:mm:ss.SSSZ")
			});
		}
	});

	plotdata_daily.forEach((d: any) => {
		for (let i = 0; i < d.src.length; i++) {
			d[d.src[i].key] = d.src[i].value;
		}
	});

	plotdata_daily.forEach((d: any) => {
		dataDaily.push({
			"time": Moment(d.key_as_string).format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
			"inverter": d.inverter || 0,
			"grid": d.grid || 0,
			"generator": d.generator || 0,
			"solar" : d.solar || 0,
			"total" : (d.inverter|| 0) + (d.grid||0) + (d.generator||0) + (d.solar || 0)
		});
	});
	let end = Moment(dataDaily[dataDaily.length - 1].time).endOf('day')
	let start = Moment(dataDaily[dataDaily.length - 1].time)
	let timeLeftToday = Moment.duration(end.diff(start))
	let remainingHours = Math.round(timeLeftToday.asHours())
	let parsedHours = []

	while (remainingHours) {
        let missingHour = Moment().add(remainingHours, 'hours').startOf('hour').format('YYYY-MM-DDTHH:mm:ss.SSSZ');
        parsedHours.push(missingHour);
        remainingHours--;
    }
	// add data for the whole day as react-vis spreads out the data to fillout the whole of the graph. 
	parsedHours.map((hour: string) => {
		dataDaily.push({
			"time": hour,
			"inverter": null,
			"grid": null,
			"generator": null,
			"solar" : null,
			"total" : null
		})
	})

	// let endofHour = Moment(parsedHours[parsedHours.length - 1]).endOf('hour')
	// let minutesToNextHour = Moment.duration(end.diff(endofHour))
	// let remainingMinutes = Math.round(minutesToNextHour.asMinutes())
	// let parsedMinutes = []

	// while (remainingMinutes) {
    //     let missingMinute = Moment().add(remainingMinutes, 'minutes').startOf('minute').format('YYYY-MM-DDTHH:mm:ss.SSSZ');
    //     parsedMinutes.push(missingMinute);
    //     remainingMinutes--;
    // }
	// parsedMinutes.map((hour: string) => {
	// 	dataDaily.push({
	// 		"time": hour,
	// 		"inverter": 0,
	// 		"grid": 0,
	// 		"generator": 0,
	// 		"solar" : 0,
	// 		"total" : 0
	// 	})
	// })
	// console.log(parsedMinutes)
	// console.log(dataDaily)


	let newDataset = ["inverter", "grid", "generator", "solar"].map(function(n) {
		return {
			key: n,
			values: dataDaily.map(function(d) {
				return {
					x: Moment(d.time),
					y: d[n],
					// y0: 0
				};
			})
		};
	});
	let newDatasetAsObject: any = {}
	newDataset.map((source: any) => {
		newDatasetAsObject[source.key] = source.values
	})
	let DailyGridPower = newDatasetAsObject['grid']
	let DailyGeneratorPower = newDatasetAsObject['generator']
	let DailyInverterPower = newDatasetAsObject['inverter']
	let DailySolarPower = newDatasetAsObject['solar']
	return Object.assign({}, state, {
		DailyGridPower,
		DailyGeneratorPower,
		DailyInverterPower,
		DailySolarPower,
		OpComplete: true,
	});
}

const applyDailyDataFetching = (state: any, action: any) => {
	return Object.assign({}, state, {
		OpComplete: false,
	});
}

const applyDailyDataErrors = (state: any, action: any) => {
	return Object.assign({}, state, {
		error: action.error
	});
}
  
function dailyDataReducer(state = INITIAL_STATE, action: any) {
	switch(action.type) {
		case DAILY_DATA_FETCHING : {
			return applyDailyDataFetching(state, action);
		}
		case DAILY_DATA_ADD : {
			return applyAddDailyData(state, action);
		}
		case DAILY_DATA_FETCH_ERROR : {
			return applyDailyDataErrors(state, action);
		}
		default : return state;
	}
}

export default dailyDataReducer;
