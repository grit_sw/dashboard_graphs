import { MONTHLY_DATA_ADD, MONTHLY_DATA_FETCH_ERROR, MONTHLY_DATA_FETCHING } from '../constants/monthlyActionTypes';
import Moment from 'moment';

const INITIAL_STATE = {
    GridData: [],
    GeneratorData: [],
    InverterData: [],
    SolarData: [],
    error: false,
    OpComplete: false,
};
  
const applyAddMonthlyData = (state: any, action: any) => {
	let configID = action.configID;
    let EnergyByDay = action.res.data.aggregations.EnergyDay_.histogram.buckets;
    let energyData: any[] = [];
    EnergyByDay.forEach(function(d: any, i: string){
        if (d.topHits.hits.hits.length >= 3) {
            let sourcesObject: any = {}
            let sources: any[] = [];
            let total = 0;
            let totalTime = 0;
            let last = Moment(d.key).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
            d.time = last
            let energy = d.topHits.hits.hits[2]._source.energyTodaySource;
            let cost = d.topHits.hits.hits[2]._source.costTodaySource;
            let time_ = d.topHits.hits.hits[2]._source.timeTodaySource;
            energy.forEach(function(e: any, i: any){
                if (sources[e.sourceType] && (e.configID_FK.toLowerCase() === configID.toLowerCase())) {
                    sources[e.sourceType].energy = e.energyToday;
                } else if ((!sources[e.sourceType]) && (e.configID_FK.toLowerCase() === configID.toLowerCase())) {
                    sources[e.sourceType] = {};
                    sources[e.sourceType].energy = e.energyToday;
                }
                total = total + (e.energyToday || 0);
            });


            cost.forEach(function(e: any, i: any){
                if (sources[e.sourceType] && (e.configID_FK.toLowerCase() === configID.toLowerCase())) {
                    sources[e.sourceType].cost = e.costToday;
                } else if ((!sources[e.sourceType]) && (e.configID_FK.toLowerCase() === configID.toLowerCase())) {
                    sources[e.sourceType] = {};
                    sources[e.sourceType].cost = e.costToday;
                }
            });
            time_.forEach(function(e: any, i: any){
                if (sources[e.sourceType] && (e.configID_FK.toLowerCase() === configID.toLowerCase())) {
                    sources[e.sourceType].time = e.timeToday;
                } else if ((!sources[e.sourceType]) && (e.configID_FK.toLowerCase() === configID.toLowerCase())) {
                    sources[e.sourceType] = {};
                    sources[e.sourceType].time = e.timeToday;
                }

                totalTime = totalTime + (e.timeToday || 0);
            });
            sourcesObject['data'] = sources;
            sourcesObject['day'] = last;
            energyData.push(sourcesObject);
            // d.counts = bucket_handlerEnergyDay(sources,1);
            // d.total = energySwitch ? total : totalTime;
        }
    });

    let AvalailableEnergy =  energyData.map((items: any) => {
        let oneDay: any = {};
        oneDay['data'] = {}
        oneDay['data']['grid'] = items.data.grid;
        oneDay['data']['generator'] = items.data.generator;
        oneDay['data']['inverter'] = items.data.inverter;
        oneDay['data']['solar'] = items.data.solar || {energy: 0, cost: 0, time: 0};
        oneDay['day'] = Moment(items.day)
        return oneDay
    })
    let daysThisMonth = Moment().daysInMonth();
    let days: Array<any> = []
    // Calculating missing days enables the width of the bar graphs remain consistent everyday of the month.
    // The days of the month that are not returned from the server (MissingDays) are filled with zeros
    while (daysThisMonth) {
        let currentDay = Moment().date(daysThisMonth).startOf('day').format('YYYY-MM-DDTHH:mm:ss.SSSZ');
        days.push(currentDay);
        daysThisMonth--;
    }
    let AvaliableDays = AvalailableEnergy.map((oneDay: any) => {
        return oneDay.day.startOf('day').format('YYYY-MM-DDTHH:mm:ss.SSSZ')
    })
    let MissingDays = days.filter(x => !AvaliableDays.includes(x));
    let MissingMonthsData = MissingDays.map((dateString: string) => {
        let oneMissingDay: any = {};
        oneMissingDay['data'] = {};
        oneMissingDay['data']['grid'] = [];
        oneMissingDay['data']['generator'] = [];
        oneMissingDay['data']['inverter'] = [];
        oneMissingDay['data']['solar'] = {energy: 0, cost: 0, time: 0};
        oneMissingDay['day'] = Moment(dateString)
        return oneMissingDay
    })
    let EnergyData = [...AvalailableEnergy, ...MissingMonthsData]
    let GridData = EnergyData.map((oneEnergy: any, index: any) => {
        let res: any = {};
        res['x'] = oneEnergy.day
        res['y'] = oneEnergy.data.grid.energy || 0
        return res
    })
    let GeneratorData = EnergyData.map((oneEnergy: any) => {
        let res: any = {};
        res['x'] = oneEnergy.day
        res['y'] = oneEnergy.data.generator.energy || 0
        return res
    })
    let InverterData = EnergyData.map((oneEnergy: any) => {
        let res: any = {};
        res['x'] = oneEnergy.day
        res['y'] = oneEnergy.data.inverter.energy || 0
        return res
    })

    let SolarData = EnergyData.map((oneEnergy: any) => {
        let res: any = {};
        res['x'] = oneEnergy.day
        res['y'] = oneEnergy.data.solar.energy || 0
        return res
    })

    // state update
    return Object.assign({}, state, {
        GridData: GridData.length > 0 ? GridData : [],
        GeneratorData: GeneratorData.length > 0 ? GeneratorData : [],
        InverterData: InverterData.length > 0 ? InverterData : [],
        SolarData: SolarData.length > 0 ? SolarData : [],
        OpComplete: true,
    });
}

const applyMonthlyDataFetching = (state: any, action: any) => {
	let prevMonthlyData = state.Monthly;
	let Monthly = Object.assign({}, prevMonthlyData, {
		OpComplete: !prevMonthlyData.OpComplete,
	})
	return Object.assign({}, state, {
		Monthly
	});
}

const applyMonthlyDataErrors = (state: any, action: any) => {
	let prevMonthlyData = state.Monthly;
	let Monthly = Object.assign({}, prevMonthlyData, {
			error: action.error,
	})
	return Object.assign({}, state, {
		Monthly
	});
}
  
function monthlyDataReducer(state = INITIAL_STATE, action: any) {
	switch(action.type) {
		case MONTHLY_DATA_FETCHING : {
			return applyMonthlyDataFetching(state, action);
		}
		case MONTHLY_DATA_ADD : {
			return applyAddMonthlyData(state, action);
		}
		case MONTHLY_DATA_FETCH_ERROR : {
			return applyMonthlyDataErrors(state, action);
		}
		default : return state;
	}
}

export default monthlyDataReducer;
