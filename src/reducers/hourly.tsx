import { HOURLY_DATA_FETCHING, HOURLY_DATA_ADD, HOURLY_DATA_FETCH_ERROR } from '../constants/dailyActionTypes';
import Moment from 'moment';

const INITIAL_STATE = {
    HourlyGridPower: [],
    HourlyGeneratorPower: [],
    HourlyInverterPower: [],
    HourlySolarPower: [],
    error: false,
    OpComplete: false,
};

const applyAddHourlyData = (state: any, action: any) => {
    let configId = action.configID;
    let DataHourly = [];
    let dataHourly: Array<any> = [];
    let plotdata_hourly = action.res.data.aggregations.Power_.histogram.buckets;
    plotdata_hourly.forEach((d: any) => {
        d.src = [];
    });
    plotdata_hourly.forEach((d: any) => {
        d.buckets = [];
        d.powerNest.configID.buckets.forEach((c: any) => {
            if (c.key.toLowerCase() === configId.toLowerCase()) {
                d.buckets = d.powerNest.SourceType.buckets;
            } else {
                d.buckets = [];
            }
        });
    });
    plotdata_hourly.forEach((d: any) => {
        let activeSources = d.activeSource.sourceType.buckets.map((bucket: any) => {
            return bucket.key;
        });
        for (let i = 0; i < d.buckets.length; i++) {
            if (activeSources.indexOf(d.buckets[i].key) != -1) {
                d.src.push({
                    "key": d.buckets[i].key,
                    "value": d.powerNest.power.avg
                });
            }
        }
    });

    plotdata_hourly.forEach((d: any) => {
        for (let i = 0; i < d.buckets.length; i++) {
            DataHourly.push({
                "key": d.buckets[i].key,
                "value": d.powerNest.power.avg || 0,
                "time": Moment(d.key_as_string).format("YYYY-MM-DDTHH:mm:ss.SSSZ")
            });
        }
    });

    plotdata_hourly.forEach((d: any) => {
        for (let i = 0; i < d.src.length; i++) {
            d[d.src[i].key] = d.src[i].value;
        }
    });

    plotdata_hourly.forEach((d: any) => {
        dataHourly.push({
            "time": Moment(d.key_as_string).format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
            "inverter": d.inverter || 0,
            "grid": d.grid || 0,
            "generator": d.generator || 0,
            "solar": d.solar || 0,
            "total": (d.inverter || 0) + (d.grid || 0) + (d.generator || 0) + (d.solar || 0)
        });
    });
    // let end = Moment(dataDaily[dataDaily.length - 1].time).endOf('day')
    // let start = Moment(dataDaily[dataDaily.length - 1].time)
    // let timeLeftToday = Moment.duration(end.diff(start))
    // let remainingHours = Math.round(timeLeftToday.asHours())
    // let parsedHours: never[] | (string | number | void | Moment.Moment | Date | (string | number)[] | Moment.MomentInputObject | undefined)[] = []

    // while (remainingHours) {
    //     let missingHour = Moment().add(remainingHours, 'hours').startOf('hour').format('YYYY-MM-DDTHH:mm:ss.SSSZ');
    //     parsedHours.push(missingHour);
    //     remainingHours--;
    // }
    // // add data for the whole day as react-vis spreads out the data to fillout the whole of the graph. 
    // parsedHours.map((hour: string) => {
    // 	dataDaily.push({
    // 		"time": hour,
    // 		"inverter": null,
    // 		"grid": null,
    // 		"generator": null,
    // 		"solar" : null,
    // 		"total" : null
    // 	})
    // })

    // let endofHour = Moment(parsedHours[parsedHours.length - 1]).endOf('hour')
    // let minutesToNextHour = Moment.duration(end.diff(endofHour))
    // let remainingMinutes = Math.round(minutesToNextHour.asMinutes())
    // let parsedMinutes = []

    // while (remainingMinutes) {
    //     let missingMinute = Moment().add(remainingMinutes, 'minutes').startOf('minute').format('YYYY-MM-DDTHH:mm:ss.SSSZ');
    //     parsedMinutes.push(missingMinute);
    //     remainingMinutes--;
    // }
    // parsedMinutes.map((hour: string) => {
    // 	dataDaily.push({
    // 		"time": hour,
    // 		"inverter": 0,
    // 		"grid": 0,
    // 		"generator": 0,
    // 		"solar" : 0,
    // 		"total" : 0
    // 	})
    // })
    // console.log(parsedMinutes)
    // console.log(dataDaily)

    if (dataHourly.length > 6) {
        // alert('gbese')
        // dataDaily.splice(0, 4)
        // return dataDaily
        console.log(dataHourly.length)
    }


    let newDataset = ["inverter", "grid", "generator", "solar"].map(function (n) {
        return {
            key: n,
            values: dataHourly.map(function (d) {
                return {
                    x: Moment(d.time),
                    y: d[n],
                    // y0: 0
                };
            })
        };
    });
    let newDatasetAsObject: any = {}
    newDataset.map((source: any) => {
        newDatasetAsObject[source.key] = source.values
    })
    let HourlyGridPower = newDatasetAsObject['grid']
    let HourlyGeneratorPower = newDatasetAsObject['generator']
    let HourlyInverterPower = newDatasetAsObject['inverter']
    let HourlySolarPower = newDatasetAsObject['solar']
    return Object.assign({}, state, {
        HourlyGridPower,
        HourlyGeneratorPower,
        HourlyInverterPower,
        HourlySolarPower,
        OpComplete: true,
    });
}

const applyHourlyDataFetching = (state: any, action: any) => {
    return Object.assign({}, state, {
        OpComplete: false,
    });
}

const applyHourlyDataErrors = (state: any, action: any) => {
    return Object.assign({}, state, {
        error: action.error
    });
}

function hourlyDataReducer(state = INITIAL_STATE, action: any) {
    switch (action.type) {
        case HOURLY_DATA_FETCHING: {
            return applyHourlyDataFetching(state, action);
        }
        case HOURLY_DATA_ADD: {
            return applyAddHourlyData(state, action);
        }
        case HOURLY_DATA_FETCH_ERROR: {
            return applyHourlyDataErrors(state, action);
        }
        default: return state;
    }
}

export default hourlyDataReducer;
