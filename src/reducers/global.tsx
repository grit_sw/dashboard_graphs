import { SAVE_CONFIG } from '../constants/globalActionTypes';

const INITIAL_STATE = {
	config: {
		id: '',
		name: 'Configuration Name'
	},
};
  
const applySaveConfig = (state: any, action: any) => {
	return Object.assign({}, state, {
		config: {
			id: action.config.id,
			name: action.config.name,
		}
	});
}
  
function globalReducer(state = INITIAL_STATE, action: any) {
	switch(action.type) {
		case SAVE_CONFIG : {
			return applySaveConfig(state, action);
		}
		default : return state;
	}
}

export default globalReducer;
