import { SAVE_USER_DETAILS } from '../constants/userActionType';

const INITIAL_STATE = {
    user: {
        name: 'Demo'
    }
}

const applySaveUserDetails = (state: any, action: any) => {
    // console.log(action)
    return Object.assign({}, state, {
        user: {
            name: action.userDetails.name
        }
    });
}

function userReducers(state = INITIAL_STATE, action: any) {
    switch(action.type) {
        case SAVE_USER_DETAILS: {
            return applySaveUserDetails(state, action);
        }
        default: return state
    }
}

export default userReducers 