import { combineReducers } from 'redux';
import globalReducer from './global';
import dailyDataReducer from './daily';
import hourlyDataReducer from './hourly';
import dataReducer from './data';
import monthlyDataReducer from './monthly';
import recentDataReducer from './recent';
import authReducer from './auth';
import userReducer from './user';
import errorReducer from './error';

const rootReducer = combineReducers({
	globalState: globalReducer,
    dailyDataState: dailyDataReducer,
    hourlyDataState: hourlyDataReducer,
	monthlyDataState: monthlyDataReducer,
	recentDataState: recentDataReducer,
	analyticsState: dataReducer,
	authState: authReducer,
	userState: userReducer,
	errorState: errorReducer
});

export default rootReducer;
