import { RECENT_DATA_ADD, RECENT_DATA_FETCH_ERROR, RECENT_DATA_FETCHING } from '../constants/recentActionTypes';
import Moment from 'moment';

const INITIAL_STATE = {
    VoltagePhaseOne: [],
    VoltagePhaseTwo: [],
    VoltagePhaseThree: [],
    CurrentPhaseOne: [],
    CurrentPhaseTwo: [],
    CurrentPhaseThree: [],
    PowerFactorOne: [],
    PowerFactorTwo: [],
    PowerFactorThree: [],
    error: false,
    OpComplete: false,
};
  
const applyAddRecentData = (state: any, action: any) => {
	let configID = action.configID;
    let PhaseData = action.res.data.hits.hits;
    let VoltagePhaseOne: Array<any> = [];
    let VoltagePhaseTwo: Array<any> = [];
    let VoltagePhaseThree: Array<any> = [];
    let CurrentPhaseOne: Array<any> = [];
    let CurrentPhaseTwo: Array<any> = [];
    let CurrentPhaseThree: Array<any> = [];
    let PowerFactorOne: Array<any> = [];
    let PowerFactorTwo: Array<any> = [];
    let PowerFactorThree: Array<any> = [];
    PhaseData.forEach(function(d: any){
        d._source.data.forEach(function(sources: any){
             let configActiveSources = sources.activeSource.filter(function(active: any){
                 return (active.configID_FK.toLowerCase() === configID.toLowerCase());
             }).map(function(activeSource: any){
                 return activeSource.sourceName;
             });
             let current1 = 0;
             let current2 = 0;
             let current3 = 0;
             let voltage1: Array<any> = [];
             let voltage2: Array<any> = [];
             let voltage3: Array<any> = [];
             let pf1: Array<any> = [];
             let pf2: Array<any> = [];
             let pf3: Array<any> = [];

             sources.voltage.forEach(function(v: any){
                 if ((v.configID_FK.toLowerCase() === configID.toLowerCase()) && (configActiveSources.indexOf(v.sourceName) != -1) ) {
                     voltage1.push(v.value[0]);
                     voltage2.push(v.value[1]);
                     voltage3.push(v.value[2]);
                 }
             });
             sources.current.map(function(current: any){
                 if ((current.configID_FK.toLowerCase() === configID.toLowerCase()) && (configActiveSources.indexOf(current.sourceName) != -1) ) {
                     current1 += current.value[0];
                     current2 += current.value[1];
                     current3 += current.value[2];
                 }
             });
             sources.powerfactor.forEach(function(pfactor: any){
                 if ((pfactor.configID_FK.toLowerCase() === configID.toLowerCase()) && (configActiveSources.indexOf(pfactor.sourceName) != -1) ) {
                     pf1.push(pfactor.value[0]);
                     pf2.push(pfactor.value[1]);
                     pf3.push(pfactor.value[2]);
                 }
             });

             CurrentPhaseOne.push({
                "x": Moment(sources.time),
                "y": current1
             });
             CurrentPhaseTwo.push({
                "x": Moment(sources.time),
                "y": current2
             });
             CurrentPhaseThree.push({
                "x": Moment(sources.time),
                "y": current3
             });
             if (voltage1.length > 0) {
                VoltagePhaseOne.push({
                    "x": Moment(sources.time),
                    "y": Math.max.apply(null, voltage1)
                });
            }
            if (voltage2.length > 0) {
                VoltagePhaseTwo.push({
                    "x": Moment(sources.time),
                    "y": Math.max.apply(null, voltage2)
                });
            }
            if (voltage3.length > 0) {
                VoltagePhaseThree.push({
                    "x": Moment(sources.time),
                    "y": Math.max.apply(null, voltage3)
                });
            }

            if (pf1.length > 0) {
                PowerFactorOne.push({
                    "x": Moment(d._source.data[0].time),
                    "y": Math.max.apply(null, pf1)
                });
            }
            if (pf2.length > 0) {
                PowerFactorTwo.push({
                    "x": Moment(d._source.data[0].time),
                    "y": Math.max.apply(null, pf2)
                });
            }
            if (pf1.length > 0) {
                PowerFactorThree.push({
                    "x": Moment(d._source.data[0].time),
                    "y": Math.max.apply(null, pf3)
                });
            }
        });
     });
    // state update
    return Object.assign({}, state, {
        VoltagePhaseOne,
        VoltagePhaseTwo,
        VoltagePhaseThree,
        CurrentPhaseOne,
        CurrentPhaseTwo,
        CurrentPhaseThree,
        PowerFactorOne,
        PowerFactorTwo,
        PowerFactorThree,
        OpComplete: true,
    });
}

const applyRecentDataFetching = (state: any, action: any) => {
	return Object.assign({}, state, {
		OpComplete: false,
	});
}

const applyRecentDataErrors = (state: any, action: any) => {
	return Object.assign({}, state, {
		error: true
	});
}
  
function recentDataReducer(state = INITIAL_STATE, action: any) {
	switch(action.type) {
		case RECENT_DATA_FETCHING : {
			return applyRecentDataFetching(state, action);
		}
		case RECENT_DATA_ADD : {
			return applyAddRecentData(state, action);
		}
		case RECENT_DATA_FETCH_ERROR : {
			return applyRecentDataErrors(state, action);
		}
		default : return state;
	}
}

export default recentDataReducer;
