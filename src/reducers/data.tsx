import { DATA_ADD, DATA_FETCH_ERROR, DATA_FETCHING } from '../constants/dataActionTypes';
import { getTimeData, getCostData, _getEnergyData, getCostPerEnergy, sortData } from './utils/data';
import Moment from 'moment';
import { analyticsHoursRemainingToday } from './utils/graphs';

const INITIAL_STATE = {
    EnergyPerSource: [],
    TimePerSource: [],
    CostPerSource: [],
    CostPerEnergy: [],
    FuelUseData: [],
    FuelRateData: [],
    BatteryCapData: [],
    BatteryDodData: [],
    error: false,
    OpComplete: false,
};
  
const applyAddAnalyticsData = (state: any, action: any) => {
    let configId = action.configID;
    let data = action.res.data.aggregations.EnergyDay_.histogram.buckets;
    let _sourceTypes: Array<any> = []; // contains source types
    let _sourceNames: Array<any> = []; // contains source names
    let _sourceNameByType: any = {}; // object with key of source types to an array of source names

    // Multilevel array of source names with top level array index representing
    // index of related source type
    let _SrcNameByType: Array<any> = [];
    let totalEnergy: Array<any> = [];
    let totalTime: Array<any> = [];
    let totalCost: Array<any> = [];

    let typeTime: any = {};
    let nameTime: any = {};
    let costName: any = {};
    let costType: any = {};
    let energyName: any = {};
    let energyType: any = {};
    data.forEach(function(d: any){
        if (d.topHits.hits.hits.length >= 3) {
            let energy = d.topHits.hits.hits[2]._source.energyTodaySource;
            let cost = d.topHits.hits.hits[2]._source.costTodaySource;
            let time = d.topHits.hits.hits[2]._source.timeTodaySource;
            energy.forEach(function(e: any){
                if (_sourceTypes.indexOf(e.sourceType) === -1 && (e.configID_FK.toLowerCase() === configId.toLowerCase())){
                    _sourceTypes.push(e.sourceType);
                    _sourceNameByType[e.sourceType]= [];
                    _SrcNameByType.push([]);
                    costType[e.sourceType] = {value:0};
                    energyType[e.sourceType] = {value:0};
                    typeTime[e.sourceType] = {value:0};
                }
                if ( _sourceNames.indexOf(e.sourceName) === -1 && (e.configID_FK.toLowerCase() === configId.toLowerCase())){
                    _sourceNames.push(e.sourceName);
                    costName[e.sourceName] = {value:0};
                    nameTime[e.sourceName] = {value:0};
                    energyName[e.sourceName] = {value:0};
                    _sourceNameByType[e.sourceType].push(e.sourceName);
                    _SrcNameByType[_sourceTypes.indexOf(e.sourceType)].push(e.sourceName);
                }
            });
            totalEnergy.push({"energyToday":energy});
            totalTime.push({"timeToday":time});
            totalCost.push({"costToday":cost});
        }
    });

    totalEnergy.forEach(function(t){
        t.energyToday.forEach(function(e: any){
            if ((e.configID_FK.toLowerCase() === configId.toLowerCase())) {
                energyType[e.sourceType].value += e.energyToday;
                energyName[e.sourceName].value += e.energyToday;
            }
        });
    });
    totalTime.forEach(function(t){
        t.timeToday.forEach(function(e: any){
        if ((e.configID_FK.toLowerCase() === configId.toLowerCase())) {
                typeTime[e.sourceType].value += e.timeToday;
                nameTime[e.sourceName].value += e.timeToday;
            }
        });
    });
    totalCost.forEach(function(t){
        t.costToday.forEach(function(e: any){
        if ((e.configID_FK.toLowerCase() === configId.toLowerCase())) {
                costType[e.sourceType].value += e.costToday;
                costName[e.sourceName].value += e.costToday;
            }
        });
    });
    let typeColor = ["inverter", "grid", "generator", "solar"];

    let timeData = getTimeData(typeTime, nameTime, typeColor);
    let costData = getCostData(costType, costName, typeColor);
    let energyData = _getEnergyData(energyType, energyName, typeColor);
    let costPerEnergyData = getCostPerEnergy(costType,costName,energyType,energyName,typeColor);

    let time_type_data = timeData.time_type_data;
    let time_name_data = timeData.time_name_data;
    let cost_type_data = costData.cost_type_data;
    let cost_name_data = costData.cost_name_data;
    let energy_type_data = energyData.energy_type_data;
    let energy_name_data = energyData.energy_name_data;
    let cost_energy_name_data = costPerEnergyData.cost_energy_name_data;
    let cost_energy_type_data = costPerEnergyData.cost_energy_type_data;

    let sortedData = sortData(_SrcNameByType, _sourceTypes, energy_name_data, energy_type_data, time_name_data, time_type_data, cost_name_data, cost_type_data,nameTime, typeColor,cost_energy_name_data,cost_energy_type_data);

    let EnergyPerSource = sortedData.EnergyPerSource;
    let TimePerSource = sortedData.TimePerSource;
    let time_type_data_sorted = sortedData.time_type_data_sorted;
    let CostPerSource = sortedData.CostPerSource;
    let cost_type_data_sorted = sortedData.cost_type_data_sorted;
    let power_name_data_sorted = sortedData.power_name_data_sorted;
    let power_type_data_sorted = sortedData.power_type_data_sorted;
    let energy_type_data_sorted = sortedData.energy_type_data_sorted;
    let nameTime_sorted = sortedData.nameTime_sorted;
    let CostPerEnergy = sortedData.CostPerEnergy;
    let cost_energy_type_data_sorted = sortedData.cost_energy_type_data_sorted;

    let fuelDataJSON = action.res.data.aggregations.fuel.fuelHistogram.buckets;
    let fuelRateJSON = action.res.data.aggregations.fuelRate.fuelRateHistogram.buckets;
    let batteryCapJSON = action.res.data.aggregations.batteryCap.batteryCapHistogram.buckets;
    let batteryDODJSON = action.res.data.aggregations.batteryDOD.DODHistogram.buckets;

    let FuelRateData: Array<any> = [];
    let FuelUseData: Array<any> = [];
    let BatteryCapData: Array<any> = [];
    let BatteryDodData: Array<any> = [];
    let sourceNames: Array<any> = [];

    // let fuelRateLines = [];
    // let fuelLines = [];
    // let batteryCapLines = [];
    // let dodLines = [];

    fuelRateJSON.forEach(function(bucket: any){
        // let time = Moment(bucket.key_as_string).format("YYYY-MM-DD HH:mm:ss");
        let time = Moment(bucket.key_as_string);
        let now = Moment();
        if ( now.isAfter(time) && (bucket.fuelRateNest.configID.buckets === undefined || bucket.fuelRateNest.configID.buckets.length === 0)) {
            sourceNames.forEach(function(sourceName){
                let data_ = {"x":time, "y":0, "sourceName":sourceName};
                FuelRateData.push(data_);
            });
        }
        bucket.fuelRateNest.configID.buckets.forEach(function(configBucket: any){
            if (configBucket.key.toLowerCase() === configId.toLowerCase()) {
                configBucket.SourceName.buckets.forEach(function(sourceBuckets: any){
                    let sourceName = sourceBuckets.key;
                    if (sourceNames.indexOf(sourceName) ===-1) {
                        sourceNames.push(sourceName);
                    }
                    let data_ = {"x":time, "y":sourceBuckets.fuelRate.avg, "sourceName":sourceName};
                    FuelRateData.push(data_);
                });
            }
        });
    });

    let fuelSum: any = {};
    fuelDataJSON.forEach(function(bucket: any){
        // let time = Moment(bucket.key_as_string).format("YYYY-MM-DD HH:mm:ss");
        let time = Moment(bucket.key_as_string);
        bucket.fuelNest.configID.buckets.forEach(function(configBucket: any){
            if (configBucket.key.toLowerCase() === configId.toLowerCase()) {
                configBucket.SourceName.buckets.forEach(function(sourceBuckets: any){
                    let sourceName = sourceBuckets.key;
                    fuelSum[sourceName] = (fuelSum[sourceName] || 0) + sourceBuckets.fuelStats.sum;
                    let totalFuel = fuelSum[sourceName];
                    let data_ = {"x":time, "y":totalFuel, "sourceName":sourceName};
                    FuelUseData.push(data_);
                });
            }
        });
    });

    batteryCapJSON.forEach(function(bucket: any){
        // let time = Moment(bucket.key_as_string).format("YYYY-MM-DD HH:mm:ss");
        let time = Moment(bucket.key_as_string);
        bucket.batteryNest.configID.buckets.forEach(function(configBucket: any){
            if (configBucket.key.toLowerCase() === configId.toLowerCase()) {
                configBucket.SourceName.buckets.forEach(function(sourceBuckets: any){
                    let sourceName = sourceBuckets.key;
                    let data_ = {"x":time, "y":sourceBuckets.batteryStats.avg, "sourceName":sourceName};
                    BatteryCapData.push(data_);
                });
            }
        });
    });

    batteryDODJSON.forEach(function(bucket: any){
        // let time = Moment(bucket.key_as_string).format("YYYY-MM-DD HH:mm:ss");
        let time = Moment(bucket.key_as_string);
        bucket.DODNest.configID.buckets.forEach(function(configBucket: any){
            if (configBucket.key.toLowerCase() === configId.toLowerCase()) {
                configBucket.SourceName.buckets.forEach(function(sourceBuckets: any){
                    let sourceName = sourceBuckets.key;
                    let data_ = {"x":time, "y":sourceBuckets.dodStats.avg, "sourceName":sourceName};
                    BatteryDodData.push(data_);
                });
            }
        });
    });

	let parsedFuelRateHours = analyticsHoursRemainingToday(FuelRateData)
	// add data for the whole day as react-vis spreads out the data to fillout the whole of the graph. 
	parsedFuelRateHours.map((hour: string) => {
		FuelRateData.push({
			"x": Moment(hour),
			"y": null,
			"sourceName": null,
		})
    })
	let parsedFuelUseHours = analyticsHoursRemainingToday(FuelUseData)
	// add data for the whole day as react-vis spreads out the data to fillout the whole of the graph. 
	parsedFuelUseHours.map((hour: string) => {
		FuelUseData.push({
			"x": Moment(hour),
			"y": null,
			"sourceName": null,
		})
    })
	let parsedBatteryCapHours = analyticsHoursRemainingToday(BatteryCapData)
	// add data for the whole day as react-vis spreads out the data to fillout the whole of the graph. 
	parsedBatteryCapHours.map((hour: string) => {
		BatteryCapData.push({
			"x": Moment(hour),
			"y": null,
			"sourceName": null,
		})
    })
	let parsedBatteryDodHours = analyticsHoursRemainingToday(BatteryDodData)
	// add data for the whole day as react-vis spreads out the data to fillout the whole of the graph. 
	parsedBatteryDodHours.map((hour: string) => {
		BatteryDodData.push({
			"x": Moment(hour),
			"y": null,
			"sourceName": null,
		})
    })
    
    // state update
    return Object.assign({}, state, {
        EnergyPerSource: EnergyPerSource.length > 0 ? EnergyPerSource : [],
        TimePerSource: TimePerSource.length > 0 ? TimePerSource : [],
        CostPerSource: CostPerSource.length > 0 ? CostPerSource : [],
        CostPerEnergy: CostPerEnergy.length > 0 ? CostPerEnergy : [],
        FuelUseData: FuelUseData.length > 0 ? FuelUseData : [],
        FuelRateData: FuelRateData.length > 0 ? FuelRateData : [],
        BatteryCapData: BatteryCapData.length > 0 ? BatteryCapData : [],
        BatteryDodData: BatteryDodData.length > 0 ? BatteryDodData : [],
        OpComplete: true,
    });
}

const applyDataFetching = (state: any, action: any) => {
	return Object.assign({}, state, {
		OpComplete: false
	});
}

const applyDataFetchErrors = (state: any, action: any) => {
	return Object.assign({}, state, {
		error: action.error
	});
}
  
function dataReducer(state = INITIAL_STATE, action: any) {
	switch(action.type) {
		// case SAVE_CONFIG_ID : {
		// 	return applySaveConfigID(state, action);
		// }
		case DATA_FETCHING : {
			return applyDataFetching(state, action);
		}
		case DATA_ADD : {
			return applyAddAnalyticsData(state, action);
		}
		case DATA_FETCH_ERROR : {
			return applyDataFetchErrors(state, action);
		}
		default : return state;
	}
}

export default dataReducer;
