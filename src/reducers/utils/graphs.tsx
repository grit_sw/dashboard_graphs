import Moment from 'moment';

export const analyticsHoursRemainingToday = (FuelRateData: any[]) => {
	let end = Moment(FuelRateData[FuelRateData.length - 1].x).endOf('day')
	let start = Moment(FuelRateData[FuelRateData.length - 1].x)
	let hoursLeftToday = Moment.duration(end.diff(start))
	let remainingHours = Math.round(hoursLeftToday.asHours())
	let parsedHours = []

	while (remainingHours) {
        let missingHour = Moment().add(remainingHours, 'hours').startOf('hour').format('YYYY-MM-DDTHH:mm:ss.SSSZ');
        parsedHours.push(missingHour);
        remainingHours--;
    }
    return parsedHours;
}