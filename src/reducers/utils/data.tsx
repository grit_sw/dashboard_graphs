const COLOR = ["lightblue", "lightgreen", "darkorange", "#FFD700","red"];

function roundTo(num: number) {
	// return +(Math.round(num + "e+2") + "e-2");
	return +(Math.round(num) + "e-2");
}


function getTimeData(typeTime: any, nameTime: any, typeColor: Array<String>) {

	let time_type_data: Array<any> = [];
	Object.keys(typeTime).forEach(function(t){
		time_type_data.push({
			"label": t,
			"angle": roundTo(typeTime[t].value),
			// "color": COLOR[typeColor.indexOf(t)]
		});
	});

	let time_name_data: Array<any> = [];
	Object.keys(nameTime).forEach(function(t){
		time_name_data.push({
			"label": t,
			"angle": roundTo(nameTime[t].value),
			// "color": COLOR[typeColor.indexOf(t)]
		});
	});

	let output: any = {};
	output['time_type_data'] = time_type_data;
	output['time_name_data'] = time_name_data;

	return output;
}

function getCostData(costType: any, costName: any, typeColor: Array<String>) {
	let cost_type_data: Array<any> = [];
	Object.keys(costType).forEach(function(t){
		cost_type_data.push({
			"label": t,
			"angle": roundTo(costType[t].value),
			// "color": COLOR[typeColor.indexOf(t)]
		});
	});
	let cost_name_data: Array<any> = [];
	Object.keys(costName).forEach(function(t){
		cost_name_data.push({
			"label": t,
			"angle": roundTo(costName[t].value),
			// "color": COLOR[typeColor.indexOf(t)]
		});
	});
	let output: any = {};
	output['cost_type_data'] = cost_type_data;
	output['cost_name_data'] = cost_name_data;

	return output;
}

function _getEnergyData (energyType: any, energyName: any, typeColor: Array<String>) {
	let energy_type_data: Array<any> = [];
	Object.keys(energyType).forEach(function(t){
		energy_type_data.push({
			"label": t,
			"angle": roundTo(energyType[t].value),
			// "color": COLOR[typeColor.indexOf(t)]
		});
	});

	let energy_name_data: Array<any> = [];
	Object.keys(energyName).forEach(function(t){
		energy_name_data.push({
			"label": t,
			"angle": roundTo(energyName[t].value),
			// "color": COLOR[typeColor.indexOf(t)]
		});
	});

	let output: any = {};
	output['energy_name_data'] = energy_name_data;
	output['energy_type_data'] = energy_type_data;
	return output;

}

function getCostPerEnergy(costType: any, costName: any, energyType: any, energyName: any, typeColor: Array<String>) {
		let cost_energy_type_data: Array<any> = [];
		Object.keys(energyType).forEach(function(t){
			cost_energy_type_data.push({
				"label": t,
				"angle": roundTo(costType[t].value / (energyType[t].value/1000) || 0),
				// "color": COLOR[typeColor.indexOf(t)]
			});
		});

		let cost_energy_name_data: Array<any> = [];
		Object.keys(costName).forEach(function(t){
			cost_energy_name_data.push({
				"label": t,
				"angle": roundTo(costName[t].value/(energyName[t].value/1000) || 0),
				// "color": COLOR[typeColor.indexOf(t)]
			});
		});

		let output: any = {};
		output['cost_energy_name_data'] = cost_energy_name_data;
		output['cost_energy_type_data'] = cost_energy_type_data;
		return output;
}

function sortData(SrcNameByType: Array<any>, SrcByType: Array<any>, SrcName_energy_data: any, energy_data: any, time_name_data: any, time_type_data: any, cost_name_data: any, cost_type_data: any, nameTime: any, typeColor: Array<String>, cost_energy_name_data: any, cost_energy_type_data: any) {

	let TimePerSource = [];
	let time_type_data_sorted = [];
	let nameTime_sorted = [];
	let cost_type_data_sorted = [];
	let CostPerSource = [];
	let power_type_data_sorted: Array<any> = [];
	let power_name_data_sorted: Array<any> = [];
	let energy_type_data_sorted = [];
	let energy_name_data_sorted = [];
	let cost_energy_type_data_sorted = [];
	let CostPerEnergy = [];

	for (let stype = 0; stype < SrcNameByType.length; stype++) {
		let col = COLOR[typeColor.indexOf(SrcByType[stype])].toString();
		for (let sname = 0; sname < SrcNameByType[stype].length; sname++) {
			for (let sname_data = 0; sname_data < time_name_data.length; sname_data++) {
				if (SrcNameByType[stype][sname] === time_name_data[sname_data].label) {
					// time_name_data[sname_data].color = col;
					TimePerSource.push(time_name_data[sname_data]);
					nameTime_sorted.push(nameTime[sname_data]);
					break;
				}
			}
			for (let sname_data = 0; sname_data < cost_name_data.length; sname_data++) {
				if (SrcNameByType[stype][sname] === cost_name_data[sname_data].label) {
					// cost_name_data[sname_data].color = col;
					CostPerSource.push(cost_name_data[sname_data]);
					break;
				}
			}
			for (let sname_data = 0; sname_data < SrcName_energy_data.length; sname_data++) {
				if (SrcNameByType[stype][sname] === SrcName_energy_data[sname_data].label) {
					// SrcName_energy_data[sname_data].color = col;
                    // SrcName_energy_data[sname_data].color = col;
					energy_name_data_sorted.push(SrcName_energy_data[sname_data]);
					break;
				}
			}
			for (let sname_data = 0; sname_data < cost_energy_name_data.length; sname_data++) {
				if (SrcNameByType[stype][sname] === cost_energy_name_data[sname_data].label) {
					// cost_energy_name_data[sname_data].color = col;
					CostPerEnergy.push(cost_energy_name_data[sname_data]);
					break;
				}
			}
		}
	}

	for (let stype = 0; stype < SrcByType.length; stype++) {
		for (let stype_data = 0; stype_data < time_type_data.length; stype_data++) {
			if (SrcByType[stype] === time_type_data[stype_data].label) {
				time_type_data_sorted.push(time_type_data[stype_data]);
				break;
			}
		}
	}

	for (let stype = 0; stype < SrcByType.length; stype++) {
		for (let stype_data = 0; stype_data < energy_data.length; stype_data++) {
			if (SrcByType[stype] === energy_data[stype_data].label) {
				energy_type_data_sorted.push(energy_data[stype_data]);
				break;
			}
		}
	}

	for (let stype = 0; stype < SrcByType.length; stype++) {
		for (let stype_data = 0; stype_data < cost_type_data.length; stype_data++) {
			if (SrcByType[stype] === cost_type_data[stype_data].label) {
				cost_type_data_sorted.push(cost_type_data[stype_data]);
				break;
			}
		}
	}

	for (let stype = 0; stype < SrcByType.length; stype++) {
		for (let stype_data = 0; stype_data < cost_energy_type_data.length; stype_data++) {
			if (SrcByType[stype] === cost_energy_type_data[stype_data].label) {
				cost_energy_type_data_sorted.push(cost_energy_type_data[stype_data]);
				break;
			}
		}
	}

    let sortedData: any = {};
	sortedData['EnergyPerSource'] = energy_name_data_sorted;
	sortedData['TimePerSource'] = TimePerSource;
	sortedData['time_type_data_sorted'] = time_type_data_sorted;
	sortedData['energy_type_data_sorted'] = energy_type_data_sorted;
	sortedData['nameTime_sorted'] = nameTime_sorted;
	sortedData['CostPerSource'] = CostPerSource;
	sortedData['cost_type_data_sorted'] = cost_type_data_sorted;
	sortedData['power_name_data_sorted'] = power_name_data_sorted;
	sortedData['power_type_data_sorted'] = power_type_data_sorted;
	sortedData['cost_energy_type_data_sorted'] = cost_energy_type_data_sorted;
	sortedData['CostPerEnergy'] = CostPerEnergy;

	return sortedData;
}

export {
    getTimeData,
    getCostData,
    _getEnergyData,
    getCostPerEnergy,
    sortData,
}