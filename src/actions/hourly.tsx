import {
    HOURLY_DATA_ADD,
    HOURLY_DATA_FETCH,
    HOURLY_DATA_FETCHING,
    HOURLY_DATA_FETCH_ERROR,
} from '../constants/dailyActionTypes';
import store from '../store';

interface IHourlyQuery {
    configId: string,
    timeStart: string,
    timeStop: string
}

const doAddHourlyData = (res: any) => ({
    type: HOURLY_DATA_ADD,
    res: res,
    configID: store.getState().globalState.config.id,
});

const doFetchHourlyData = (query: IHourlyQuery) => ({
    type: HOURLY_DATA_FETCH,
    query,
});

const doFetchingHourlyData = () => ({
    type: HOURLY_DATA_FETCHING,
    // query,
});

const doFetchErrorHourlyData = (error: any) => ({
    type: HOURLY_DATA_FETCH_ERROR,
    error,
});
  
export {
    doAddHourlyData,
    doFetchHourlyData,
    doFetchingHourlyData,
    doFetchErrorHourlyData,
};
