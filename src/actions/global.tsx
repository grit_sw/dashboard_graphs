import { SAVE_CONFIG } from '../constants/globalActionTypes';
import { FETCH_CONFIG } from '../constants/globalActionTypes';

const doSaveConfig = (config: any) => ({
    type: SAVE_CONFIG,
    config,
});

const doFetchConfig = (urlType: string) => ({
    type: FETCH_CONFIG,
    urlType
})

export {
    doSaveConfig,
    doFetchConfig,
};
