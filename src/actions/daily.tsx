import {
    DAILY_DATA_ADD,
    DAILY_DATA_FETCH,
    DAILY_DATA_FETCHING,
    DAILY_DATA_FETCH_ERROR,
} from '../constants/dailyActionTypes';
import store from '../store';

interface IDailyQuery {
    configId: string,
    timeStart: string,
    timeStop: string
}

const doAddDailyData = (res: any) => ({
    type: DAILY_DATA_ADD,
    res: res,
    configID: store.getState().globalState.config.id,
});

const doFetchDailyData = (query: IDailyQuery) => ({
    type: DAILY_DATA_FETCH,
    query,
});

const doFetchingDailyData = () => ({
    type: DAILY_DATA_FETCHING,
    // query,
});

const doFetchErrorDailyData = (error: any) => ({
    type: DAILY_DATA_FETCH_ERROR,
    error,
});
  
export {
    doAddDailyData,
    doFetchDailyData,
    doFetchingDailyData,
    doFetchErrorDailyData,
};
