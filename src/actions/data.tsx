import {
    DATA_ADD,
    DATA_FETCH,
    DATA_FETCHING,
    DATA_FETCH_ERROR,
} from '../constants/dataActionTypes';
import store from '../store';

interface IDataQuery {
    configId: string,
    timeStart: string,
    timeStop: string
}

const doAddAnalyticsData = (res: any) => ({
    type: DATA_ADD,
    res: res,
    configID: store.getState().globalState.config.id
});

const doFetchAnalyticsData = (query: IDataQuery) => ({
    type: DATA_FETCH,
    query,
});

const doFetchingAnalyticsData = () => ({
    type: DATA_FETCHING,
    // query,
});

const doFetchErrorAnalyticsData = (error: any) => ({
    type: DATA_FETCH_ERROR,
    error: true,
});
  
export {
    doAddAnalyticsData,
    doFetchAnalyticsData,
    doFetchingAnalyticsData,
    doFetchErrorAnalyticsData,
};
