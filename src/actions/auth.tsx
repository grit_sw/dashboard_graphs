import { CHANGE_AUTH } from '../constants/authActionType'

const doChangeAuth = (auth: any) => ({
    type: CHANGE_AUTH,
    auth,
})

export {
    doChangeAuth
}