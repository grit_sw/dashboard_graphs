import {
    RECENT_DATA_ADD,
    RECENT_DATA_FETCH,
    RECENT_DATA_FETCHING,
    RECENT_DATA_FETCH_ERROR,
} from '../constants/recentActionTypes';
import store from '../store';

interface IRecentQuery {
    configId: string,
    timeStart: string,
    timeStop: string
}

const doAddRecentData = (res: any) => ({
    type: RECENT_DATA_ADD,
    res: res,
    configID: store.getState().globalState.config.id,
});

const doFetchRecentData = (query: IRecentQuery) => ({
    type: RECENT_DATA_FETCH,
    query,
});

const doFetchingRecentData = () => ({
    type: RECENT_DATA_FETCHING,
    // query,
});

const doFetchErrorRecentData = (error: any) => ({
    type: RECENT_DATA_FETCH_ERROR,
    error,
});
  
export {
    doAddRecentData,
    doFetchRecentData,
    doFetchingRecentData,
    doFetchErrorRecentData,
};
