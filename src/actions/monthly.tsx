import {
    MONTHLY_DATA_ADD,
    MONTHLY_DATA_FETCH,
    MONTHLY_DATA_FETCHING,
    MONTHLY_DATA_FETCH_ERROR,
} from '../constants/monthlyActionTypes';
import store from '../store';

interface IMonthlyQuery {
    configId: string,
    timeStart: string,
    timeStop: string
}

const doAddMonthlyData = (res: any) => ({
    type: MONTHLY_DATA_ADD,
    res: res,
    configID: store.getState().globalState.config.id,
});

const doFetchMonthlyData = (query: IMonthlyQuery) => ({
    type: MONTHLY_DATA_FETCH,
    query,
});

const doFetchingMonthlyData = () => ({
    type: MONTHLY_DATA_FETCHING,
    // query,
});

const doFetchErrorMonthlyData = (error: any) => ({
    type: MONTHLY_DATA_FETCH_ERROR,
    error,
});
  
export {
    doAddMonthlyData,
    doFetchMonthlyData,
    doFetchingMonthlyData,
    doFetchErrorMonthlyData,
};
