import { SAVE_USER_DETAILS } from '../constants/userActionType';

let doSaveUserDetails = (userDetails: any) =>( {
    type: SAVE_USER_DETAILS,
    userDetails
})

export {
    doSaveUserDetails,
}