import { FETCH_ERROR } from '../constants/errorActionType';
import { ADD_ERROR } from '../constants/errorActionType';

const doFetchError = (error: any) => ({
    type: FETCH_ERROR,
    error
})

const doSaveError = (error: any) => ({
    type: ADD_ERROR,
    error
})


export {
    doFetchError,
    doSaveError
}