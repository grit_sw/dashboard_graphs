const powerSourcesColors = {
	'grid': '#033F63',
	'generator': '#F5761A',
	'inverter': '#79B791',
	'solar': '#F7EBEC',
};

export default powerSourcesColors;
