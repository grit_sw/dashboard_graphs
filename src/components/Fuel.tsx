import React from 'react';
import Line from './Line';
import { withStyles } from '@material-ui/core';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import { getAnalyticsData, getAnalyticsDataError } from '../selectors/data';
import { connect } from 'react-redux';


const styles = (theme: any) => ({
	container: {
		display: 'flex',
		justifyContent: 'center',
        width: '100%',
	},
	column: {
        flexGrow: 1,
		justifyContent: 'center',
	},
	heading: {
		fontSize: theme.typography.pxToRem(18),
	},
    panel: {
        width: "100%",
    },
    details: {
        flexGrow: 1,
        // flexDirection: "column" as "column",
        flexWrap: "wrap" as "wrap", 
        justifyContent: 'space-around',
        width: "100%",
    },
})

interface IFuelProps {
    classes: any; // added by material-ui withStyles css
    FuelUseData: any;
    FuelRateData: any;
    OpComplete: boolean;
    error: any;
}
interface IDimensions {
    width: Number,
    height: Number 
}

interface IFuelState {
    graph: IDimensions,
}


class Fuel extends React.Component<IFuelProps, IFuelState> {
    constructor(props: IFuelProps) {
        super(props)
        this.state = {
            graph: {
                width: 600,
                height: 300
            }
        }
    }

    render() {
        const { classes, FuelUseData, FuelRateData, OpComplete, error } = this.props;
        return (
			<div className={classes.container}>
                <ExpansionPanel className={classes.panel} defaultExpanded>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <div className={classes.column}>
                            <Typography className={classes.heading}>Fuel</Typography>
                        </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails className={classes.details}>
                        <Line 
                            graph={this.state.graph} 
                            xAxisName={'Time'} 
                            yAxisName={'Fuel Use'}
                            Data={FuelUseData}
                            OpComplete={OpComplete}
                            error={error}
                        />
                        <Line
                            graph={this.state.graph} 
                            xAxisName={'Time'} 
                            yAxisName={'Fuel Rate'} 
                            Data={FuelRateData}
                            OpComplete={OpComplete}
                            error={error}
                        />
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </div>
        );
    }
}
            

const mapStateToProps = (state: any) => {
    let analyticsState = getAnalyticsData(state)
    return {
        FuelUseData: analyticsState.FuelUseData,
        FuelRateData: analyticsState.FuelRateData,
        OpComplete: analyticsState.OpComplete,
        error: getAnalyticsDataError(state),
}};

export default connect(mapStateToProps)(withStyles(styles, { withTheme: true })(Fuel));
