import React, { Component } from 'react';
import Moment from 'moment';
import { format } from 'd3-format';
import color from '../colors/colors'
import {
    XYPlot,
    XAxis,
    YAxis,
    VerticalGridLines,
    HorizontalGridLines,
    VerticalBarSeries,
    VerticalBarSeriesCanvas,
    Hint,
    ChartLabel
} from 'react-vis';
import { ScaleLoader } from 'react-spinners';
import { connect } from 'react-redux';
import { getConfig } from '../selectors/global';
import { doFetchDailyData, doFetchingDailyData } from '../actions/daily';
import { getMonthlyData, getMonthlyDataError } from '../selectors/monthly';
import { doFetchAnalyticsData, doFetchingAnalyticsData } from '../actions/data';
import Loading from './Loader';
// import { doSaveConfig } from '../actions/daily';

const tipStyle = {
    display: 'flex',
    color: '#fff',
    background: '#000',
    alignItems: 'center',
    padding: '5px'
};

const boxStyle = { height: '10px', width: '10px' };

interface IDimensions {
    width: number,
    height: number
}

interface IStackedBarStyleProps {
    graph: IDimensions;
    GridData: Array<any>,
    GeneratorData: Array<any>,
    InverterData: Array<any>,
    SolarData: Array<any>,
    configID: string,
    onFetchDailyData: any,
    onFetchAnalyticsData: any,
    setDailyFetching: any,
    setAnalyticsFetching: any
    OpComplete: boolean,
}

interface IStackedBarStyleValues {
    DailyGridEnergy: Array<any>;
    hoveredCell: any
}

class StackedBar extends Component<IStackedBarStyleProps, IStackedBarStyleValues> {
    constructor(props: IStackedBarStyleProps) {
        super(props);
        this.state = {
            DailyGridEnergy: [{}],
            hoveredCell: null
        }
    }

    onValueClick = (d: any, e: any) => {
        let params = {
            configId: this.props.configID,
            timeStart: d.x.startOf('day').format("YYYY-MM-DDTHH:mm:ss.SSSSZ"),
            timeStop: d.x.endOf('day').format("YYYY-MM-DDTHH:mm:ss.SSSSZ"),
        }
        this.props.setDailyFetching()
        this.props.setAnalyticsFetching()
        this.props.onFetchDailyData(params)
        this.props.onFetchAnalyticsData(params)
    }

    saveValue = (datapoint: any, event: any) => {
        console.log(datapoint)
        console.log(event)
        this.setState({ hoveredCell: datapoint }, () => {
            console.log(this.state.hoveredCell)
        })
        console.log(this.state.hoveredCell)
    }

    render() {
        const { graph, GridData, GeneratorData, InverterData, configID, onFetchDailyData, OpComplete } = this.props;
        const { hoveredCell } = this.state;
        // const BarSeries = VerticalBarSeries;
        const GridSeries = VerticalBarSeries;
        const GeneratorSeries = VerticalBarSeries;
        const InverterSeries = VerticalBarSeries;
        if (OpComplete) {
            return (
                <XYPlot
                    width={graph.width}
                    height={graph.height}
                    stackBy='y'
                    margin={{ left: 75 }}
                >
                    <VerticalGridLines />
                    <HorizontalGridLines />
                    <XAxis
                        // title='Day'
                        xType="time"
                        // xDomain={[Moment().startOf('month'), Moment().endOf('month')]}
                        style={{
                            text: { stroke: 'none', fill: '#6b6b76', fontWeight: 600 }
                        }}
                    />
                    <YAxis
                        // title='Energy (Wh)'
                        tickFormat={(v: number) => `${format(".3s")(v)} Wh`}
                    />
                    <ChartLabel
                        text="Energy (Wh)"
                        className="alt-y-label"
                        includeMargin={false}
                        xPercent={-0.06}
                        yPercent={0.5}
                        style={{
                            transform: 'rotate(-90)',
                            textAnchor: 'end'
                        }}
                    />
                    <ChartLabel
                        text="Day"
                        className="alt-x-label"
                        includeMargin={false}
                        xPercent={0.5}
                        yPercent={1.19}
                    />
                    <GridSeries
                        color={color.grid}
                        onValueClick={this.onValueClick}
                        data={GridData}
                        onValueMouseOver={this.saveValue}
                        onValueMouseOut={(v: any) => this.setState({ hoveredCell: false })}
                    />

                    <GeneratorSeries
                        color={color.generator}
                        onValueClick={this.onValueClick}
                        data={GeneratorData}
                        onValueMouseOver={this.saveValue}
                        onValueMouseOut={(v: any) => this.setState({ hoveredCell: null })}
                    />
                    <InverterSeries
                        color={color.inverter}
                        onValueClick={this.onValueClick}
                        data={InverterData}
                        onValueMouseOver={this.saveValue}
                        onValueMouseOut={(v: any) => this.setState({ hoveredCell: null })}
                    />
                    {hoveredCell ? (
                        <Hint
                            value={hoveredCell}
                            align={{horizontal: Hint.ALIGN.AUTO, vertical: Hint.ALIGN.AUTO}}
                        >
                            <div
                                style={{
                                    fontSize: 14,
                                    width: '80px',
                                    backgroundColor: "#000000",
                                    color: '#FFFFFF'
                                }}
                            >
                                {`${hoveredCell.x}, ${Number((hoveredCell.y).toFixed(3))}`}
                            </div>
                        </Hint>
                    ) : null}
                </XYPlot>
            );
        } else {
            return (
                <Loading
                    height={graph.height}
                    width={graph.width}
                />
            )
        }
    }
}

const mapStateToProps = (state: any) => {
    let monthlyState = getMonthlyData(state)
    return {
        configID: getConfig(state).id,
        GridData: monthlyState.GridData,
        GeneratorData: monthlyState.GeneratorData,
        InverterData: monthlyState.InverterData,
        SolarData: monthlyState.SolarData,
        OpComplete: monthlyState.OpComplete,
        error: getMonthlyDataError(state),
    }
};

const mapDispatchToProps = (dispatch: any) => ({
    onFetchDailyData: (query: any) => {
        dispatch(doFetchDailyData(query))
    },
    onFetchAnalyticsData: (query: any) => {
        dispatch(doFetchAnalyticsData(query))
    },
    setDailyFetching: () => {
        dispatch(doFetchingDailyData())
    },
    setAnalyticsFetching: () => {
        dispatch(doFetchingAnalyticsData())
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(StackedBar);
