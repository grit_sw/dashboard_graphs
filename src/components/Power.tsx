import React from 'react';
import { withStyles, Button, Grid } from '@material-ui/core';
import PowerAreaSeries from './PowerAreaSeries';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import PowerAreaSeries2 from './PowerAreaSeries2';
import Switch from '@material-ui/core/Switch';

const styles = (theme: any) => ({
    container: {
        display: 'flex',
        justifyContent: 'center',
        width: '100%',
        height: "500px"
    },
    heading: {
        fontSize: theme.typography.pxToRem(18),
    },
    column: {
        flexGrow: 1,
        justifyContent: 'center',
    },

    button: {
        // width: '50%',
        // height: 'auto',
        borderRadius: '50%',
        // fontSize: '2px',
        size: 'small'
    }
})

const AntSwitch = withStyles(theme => ({
    root: {
        width: 28,
        height: 16,
        padding: 0,
        display: 'flex',
    },
    switchBase: {
        padding: 2,
        color: theme.palette.grey[500],
        '&$checked': {
            transform: 'translateX(12px)',
            color: theme.palette.common.white,
            '& + $track': {
                opacity: 1,
                backgroundColor: theme.palette.primary.main,
                borderColor: theme.palette.primary.main,
            },
        },
    },
    thumb: {
        width: 12,
        height: 12,
        boxShadow: 'none',
    },
    track: {
        border: `1px solid ${theme.palette.grey[500]}`,
        borderRadius: 16 / 2,
        opacity: 1,
        backgroundColor: theme.palette.common.white,
    },
    checked: {},
}))(Switch);

interface IPowerProps {
    classes: any; // added by material-ui withStyles css
}


interface IDimensions {
    width: number,
    height: number
}

interface IPowerState {
    graph: IDimensions
    choice: boolean
}


class Power extends React.Component<IPowerProps, IPowerState> {
    constructor(props: IPowerProps) {
        super(props)
        this.state = {
            graph: {
                width: 1200,  // todo: set the value to be 100% of the parent container in integer.
                height: 300,
            },
            choice: false
        }
    }

    render() {
        const { classes } = this.props;
        const style = this.state;

        // @ts-ignore
        const handleChange = name => event => {
            this.setState({ ...this.state, [name]: event.target.checked });
        };

        return (
            <div className={classes.container}>
                <ExpansionPanel defaultExpanded>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <div className={classes.column}>
                            <Typography className={classes.heading}>Power</Typography>
                        </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails className={classes.details}>
                        <div style={{ display: 'flex', flexDirection: 'column' }}>
                            {/* <div style={{ backgroundColor: '#CCCCCC', marginLeft: '50vw', borderRadius: '20px' }}>
                                <Button
                                    size='small'
                                    className={classes.button}
                                    onClick={() => { this.setState(prevState => ({ choice: !prevState.choice })) }}
                                    style={{ backgroundColor: '#CCCCCC', marginLeft: '0vw' }}
                                >
                                    toggle graph
                                </Button>
                            </div> */}
                            <Typography component="div">
                                <Grid component="label" container justify="flex-end" alignItems="center" spacing={1}>
                                    <Grid item>Daily</Grid>
                                    <Grid item>
                                        <AntSwitch
                                            checked={this.state.choice}
                                            onChange={() => { this.setState(prevState => ({ choice: !prevState.choice })) }}
                                            value="choice"
                                        />
                                    </Grid>
                                    <Grid item>Hourly</Grid>
                                </Grid>
                            </Typography>
                            <div>
                                {!this.state.choice ?
                                    (
                                        <PowerAreaSeries
                                            graph={style.graph}
                                        />
                                    ) : (
                                        <PowerAreaSeries2
                                            graph={style.graph}
                                        />
                                    )
                                }
                            </div>
                        </div>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </div>
        );
    }
}

export default withStyles(styles, { withTheme: true })(Power);
