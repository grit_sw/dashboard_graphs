import React from 'react';
import { withStyles } from '@material-ui/core';
import PowerAreaSeries2 from './PowerAreaSeries2';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';

const styles = (theme: any) => ({
	container: {
        display: 'flex',
        justifyContent: 'center',
        width: '100%',
        height: "60vh"
    },
	heading: {
		fontSize: theme.typography.pxToRem(18),
	},
	column: {
        flexGrow: 1,
		justifyContent: 'center',
	},
})

interface IPower2Props {
	classes: any; // added by material-ui withStyles css
}


interface IDimensions {
	width: number,
	height: number
}

interface IPower2State {
	graph: IDimensions
}


class Power2 extends React.Component<IPower2Props, IPower2State> {
    constructor(props: IPower2Props) {
        super(props)
		this.state = {
			graph: {
				width: 1200,  // todo: set the value to be 100% of the parent container in integer.
				height: 300
			}
		}
    }

    render() {
        const { classes } = this.props;
		const style = this.state;
        return (
            <div className={classes.container}>
				<ExpansionPanel defaultExpanded>
					<ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
						<div className={classes.column}>
							<Typography className={classes.heading}>Power</Typography>
						</div>
					</ExpansionPanelSummary>
					<ExpansionPanelDetails className={classes.details}>
                        <PowerAreaSeries2
							graph={style.graph}
						/>
                    </ExpansionPanelDetails>
				</ExpansionPanel>
            </div>
            );
        }
    }
				
export default withStyles(styles, { withTheme: true })(Power2);