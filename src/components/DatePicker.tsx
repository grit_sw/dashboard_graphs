import React, { useState } from "react";
import { DateTimePicker, KeyboardDateTimePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";

import DateFnUtils from "@date-io/date-fns";

function InlineDateTimePicker(props: any) {

  return (
    <MuiPickersUtilsProvider utils={DateFnUtils}>
      <KeyboardDateTimePicker
        variant="inline"
        ampm={false}
        // label="With keyboard"
        value={props.value}
        onChange={props.onChange}
        onError={console.log}
        // disablePast
        format="MM/dd/yyyy HH:mm"
      />
    </MuiPickersUtilsProvider>
  );
}

export default InlineDateTimePicker;