import React from 'react';
import { withStyles } from '@material-ui/core';
import Moment from 'moment';
import { format } from 'd3-format';
import 'react-vis/dist/style.css';

import {
	XYPlot,
	XAxis,
	YAxis,
	HorizontalGridLines,
	VerticalGridLines,
	LineSeries,
} from 'react-vis';
import Loading from './Loader';

const styles = () => ({
	container: {
		display: 'flex',
		justifyContent: 'center',
	},
})

interface IDimensions {
	width: Number,
	height: Number
}

interface ILineProps {
	classes: any; // added by material-ui withStyles css
	// props: any; // added by material-ui withStyles css
	xAxisName: string;
	yAxisName: string;
	graph: IDimensions;
	Data: any;
	OpComplete: any;
	error: any;
}


class Line extends React.Component<ILineProps> {
	constructor(props: ILineProps) {
		super(props)
	}

	render() {
		const { graph, classes, Data, OpComplete, error } = this.props;
        if (OpComplete) {
			return (
				<div className={classes.container}>
					<XYPlot 
						width={graph.width} 
						height={graph.height}
						margin={{
							right: 75,
							left: 75,
						}}
					>
						<HorizontalGridLines style={{stroke: '#B7E9ED'}} />
						{/* <VerticalGridLines style={{stroke: '#B7E9ED'}} /> */}
						<XAxis
							title={this.props.xAxisName}
							xType="time"
							// xDomain={[Moment().startOf('day'), Moment().endOf('day')]}
							style={{
								line: {stroke: '#ADDDE1'},
								ticks: {stroke: '#ADDDE1'},
								text: {stroke: 'none', fill: '#6b6b76', fontWeight: 600}
							}}
						/>
						<YAxis 
							margin={100}
							title={this.props.yAxisName}
							// tickFormat={(v: number) => `${format(".3s")(v)}`}
						/>
						<LineSeries
							className="first-series"
							getNull={(d: any) => d.y !== null}
							data={ Data }
						/>
					</XYPlot>
				</div>
			);
        } else {
            return (
                <Loading
					height={graph.height}
					width={graph.width}
				/>
            )
        }
	}
}

export default withStyles(styles, { withTheme: true })(Line);
