import React, { Component } from 'react';
import Moment from 'moment';
import { format } from 'd3-format';
import { connect } from 'react-redux';
import { getHourlyData, getHourlyDataError } from '../selectors/daily';
import { ScaleLoader } from 'react-spinners';
import {
    XYPlot,
    XAxis,
    YAxis,
    VerticalGridLines,
    HorizontalGridLines,
    AreaSeries,
    Hint,
    MarkSeries,
    DiscreteColorLegend,
    Highlight,
    ChartLabel
} from 'react-vis';
import color from '../colors/colors';
import Loading from './Loader';
import  MultiPhase from './MultiPhase';
// import DiscreteColorLegend from 'legends/discrete-color-legend';

interface IDimensions {
    width: number,
    height: number
}

interface PowerIAreaSeries2Props {
    graph: IDimensions
    HourlyGridPower: Array<object>,
    HourlyGeneratorPower: Array<object>,
    HourlyInverterPower: Array<object>,
    HourlySolarPower: Array<object>,
    OpComplete: Boolean,
}

interface PowerIAreaSeries2States {
    value: any
    value2: any
    value3: any
    value4: any
    hoveredItem: any
    filter: any
}

const ITEMS = [
    'grid',
    'generator',
    'inverter',
    'solar'
];

const COLORS = [
    '#033F63',
    '#F5761A',
    '#79B791',
    '#F7EBEC',
];

class PowerAreaSeries2 extends Component<PowerIAreaSeries2Props, PowerIAreaSeries2States> {
    constructor(props: PowerIAreaSeries2Props) {
        super(props);

        this.state = {
            value: null,
            value2: null,
            value3: null,
            value4: null,
            hoveredItem: false,
            filter: null
        };
    }

    _forgetValue = () => {
        this.setState({
            value: null,
            value2: null,
            value3: null,
            value4: null,
            hoveredItem: false
        });
    };

    _rememberValue = (value: any) => {
        console.log(value)
        this.setState({ value });
    };

    _rememberValue2 = (value2: any) => {
        console.log(value2)
        this.setState({ value2 });
    };

    _rememberValue3 = (value3: any) => {
        console.log(value3)
        this.setState({ value3 });
    };

    _rememberValue4 = (value4: any) => {
        console.log(value4)
        this.setState({ value4 });
    };

    render() {
        const { value, value2, value3, value4, hoveredItem } = this.state;
        const { graph, HourlyGridPower, HourlyGeneratorPower, HourlyInverterPower, HourlySolarPower, OpComplete } = this.props;
        // if (!(Object.entries(DailyGridPower[0]).length === 0 && DailyGridPower[0].constructor === Object)) {
        if (OpComplete) {
            return (
                <XYPlot
                    width={graph.width}
                    height={graph.height}
                    margin={{ left: 75 }}
                >
                    {/* <VerticalGridLines /> */}
                    <HorizontalGridLines />
                    <XAxis
                        // title='Time(Hour)'
                        xType="time"
                    // xDomain={[Moment().startOf('day'), Moment().endOf('day')]}
                    // style={{
                    //     text: { stroke: 'none', fill: '#6b6b76', fontWeight: 600 }
                    // }}
                    />
                    <YAxis
                        // left={}
                        // title='Power (W)'
                        tickFormat={(v: number) => `${format(".3s")(v)}W`}
                    />
                    <ChartLabel
                        text="Power (W)"
                        className="alt-y-label"
                        includeMargin={false}
                        xPercent={-0.06}
                        yPercent={0.5}
                        style={{
                            transform: 'rotate(-90)',
                            textAnchor: 'end'
                        }}
                    />
                    <ChartLabel
                        text="Time(Minutes)"
                        className="alt-x-label"
                        includeMargin={false}
                        xPercent={0.5}
                        yPercent={1.2}
                    />
                    <AreaSeries
                        className="area-series-example"
                        curve="curveLinear"  // See https://github.com/d3/d3-shape#curves for options.
                        getNull={(d: any) => d.y !== null}
                        color={color.grid}
                        // onNearestX={this._rememberValue}
                        onNearestX={this._rememberValue}
                        // onSeriesMouseOut={this._forgetValue}
                        data={HourlyGridPower}
                    />
                    <AreaSeries
                        className="area-series-example"
                        curve="curveLinear"  // See https://github.com/d3/d3-shape#curves for options.
                        getNull={(d: any) => d.y !== null}
                        data={HourlyGeneratorPower}
                        color={color.generator}
                        onNearestX={this._rememberValue2}
                    />
                    <AreaSeries
                        className="area-series-example"
                        curve="curveLinear"  // See https://github.com/d3/d3-shape#curves for options.
                        getNull={(d: any) => d.y !== null}
                        data={HourlyInverterPower}
                        onNearestX={this._rememberValue3}
                        color={color.inverter}
                    />
                    <AreaSeries
                        className="area-series-example"
                        curve="curveLinear"  // See https://github.com/d3/d3-shape#curves for options.
                        getNull={(d: any) => d.y !== null}
                        data={HourlySolarPower}
                        onNearestX={this._rememberValue4}
                        color={color.solar}
                    />
                    {/* <Highlight
                        drag
                        enableX={false}
                        onBrush={(area: any) => this.setState({ filter: area })}
                        onDrag={(area: any) => this.setState({ filter: area })}
                    /> */}
                    <DiscreteColorLegend
                        colors={COLORS}
                        // @ts-ignore
                        onItemMouseEnter={i => this.setState({ hoveredItem: i })}
                        onItemMouseLeave={() => this.setState({ hoveredItem: false })}
                        orientation="horizontal"
                        width={300}
                        items={ITEMS}
                    />
                    {!value ?
                        null
                        : (
                            value.y === 0 || value.y === null ?
                                null
                                :
                                <Hint
                                    value={value}
                                    style={{
                                        fontSize: 14,
                                        text: {
                                            display: 'none'
                                        },
                                        value: {
                                            color: '#FFFFFF'
                                        }
                                    }}
                                >
                                    <div
                                        style={{
                                            fontSize: 14,
                                            width: '250px',
                                            backgroundColor: "#000000",
                                            // text: {
                                            //     display: 'none'
                                            // },
                                            color: '#FFFFFF'
                                        }}
                                    >
                                        {
                                            <div>
                                                <div>{`${value.x}`}</div>
                                                <div style={{ textAlign: "left" }}>Grid</div>
                                                <div style={{ textAlign: "left" }}>{`${Number((value.y).toFixed(3))}`}</div>
                                            </div>
                                        }
                                    </div>
                                </Hint>
                        )
                    }
                    {!value2 ?
                        null
                        : (
                            value2.y === 0 || value2.y === null ?
                                null
                                :
                                <Hint
                                    value={value2}
                                    style={{
                                        fontSize: 14,
                                        text: {
                                            display: 'none'
                                        },
                                        value: {
                                            color: 'red'
                                        }
                                    }}
                                >
                                    <div
                                        style={{
                                            fontSize: 14,
                                            width: '250px',
                                            backgroundColor: "#000000",
                                            // text: {
                                            //     display: 'none'
                                            // },
                                            color: '##FFFFFF'
                                        }}
                                    >
                                        {
                                            <div>
                                                <div>{`${value2.x}`}</div>
                                                <div style={{ textAlign: "left" }}>Generator</div>
                                                <div style={{ textAlign: "left" }}>{`${Number((value2.y).toFixed(3))}`}</div>
                                            </div>
                                        }
                                    </div>
                                </Hint>
                        )
                    }
                    {!value3 ?
                        null
                        : (
                            value3.y === 0 || value3.y === null ?
                                null
                                :
                                <Hint
                                    value={value3}
                                    style={{
                                        fontSize: 14,
                                        text: {
                                            display: 'none'
                                        },
                                        value: {
                                            color: 'red'
                                        }
                                    }}
                                >
                                    <div
                                        style={{
                                            fontSize: 14,
                                            width: '250px',
                                            backgroundColor: "#000000",
                                            // text: {
                                            //     display: 'none'
                                            // },
                                            color: '#FFFFFF'
                                        }}
                                    >
                                        {
                                            <div>
                                                <div>{`${value3.x}`}</div>
                                                <div style={{ textAlign: "left" }}>Inverter</div>
                                                <div style={{ textAlign: "left" }}>{`${Number((value3.y).toFixed(3))}`}</div>
                                            </div>
                                        }
                                    </div>
                                </Hint>
                        )
                    }
                    {!value4 ?
                        null
                        : (
                            value4.y === 0 || value4.y === null ?
                                null
                                :
                                <Hint
                                    value={value4}
                                    style={{
                                        fontSize: 14,
                                        text: {
                                            display: 'none'
                                        },
                                        value: {
                                            color: 'red'
                                        }
                                    }}
                                >
                                    <div
                                        style={{
                                            fontSize: 14,
                                            width: '250px',
                                            backgroundColor: "#000000",
                                            // text: {
                                            //     display: 'none'
                                            // },
                                            color: 'red'
                                        }}
                                    >
                                        {
                                            <div>
                                                <div>{`${value4.x}`}</div>
                                                <div style={{ textAlign: "left" }}>Solar</div>
                                                <div style={{ textAlign: "left" }}>{`${Number((value4.y).toFixed(3))}`}</div>
                                            </div>
                                        }
                                    </div>
                                </Hint>
                        )
                    }
                </XYPlot>
            );
        } else {
            return (
                <Loading
                    height={`${graph.height}px`}
                    width={`${graph.width}px`}
                />
            )
        }
    }
}

const mapStateToProps = (state: any) => {
    let hourlyData = getHourlyData(state)
    return {
        HourlyGridPower: hourlyData.HourlyGridPower,
        HourlyGeneratorPower: hourlyData.HourlyGeneratorPower,
        HourlyInverterPower: hourlyData.HourlyInverterPower,
        HourlySolarPower: hourlyData.HourlySolarPower,
        OpComplete: hourlyData.OpComplete,
        error: getHourlyDataError(state),
    }
};

export default connect(mapStateToProps)(PowerAreaSeries2);
