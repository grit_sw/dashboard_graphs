import React from 'react';
import color from '../colors/colors'

import { RadialChart } from 'react-vis';

export default function Pie(props: any) {
	return (
		<RadialChart
			margin={{top: 100}}
			data={props.Data}
			labelsRadiusMultiplier={1.1}
			colorRange={[color.generator, color.inverter, color.grid]}
			// colorRange={[color.generator, color.grid, color.inverter, color.solar]}
			labelsStyle={{fontSize: 16, fill: '#222'}}
			style={{stroke: '#fff', strokeWidth: 2}}
			width={150}
            height={150}
		>
		</RadialChart>
	);
}
