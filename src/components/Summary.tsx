import React, { Component } from 'react';
import PieDonut from './PieDonut';
import { withStyles } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import { connect } from 'react-redux';
import { getAnalyticsData, getAnalyticsDataError } from '../selectors/data';
import Loading from './Loader';
import {
    DiscreteColorLegend,
} from 'react-vis';


const styles = (theme: any) => ({
    container: {
        display: 'flex',
        justifyContent: 'center',
        // flexWrap: "wrap" as "wrap", 
        // flexDirection: "row" as "row",
        width: '100%',
        // height: '50%',
    },
    column: {
        flexGrow: 1,
        justifyContent: 'center',
    },
    child: {
        display: 'flex',
        // flexGrow: 1,
        flexBasis: '50%',
        width: "100%",
        // // flexDirection: "column" as "column",
        justifyContent: 'center',
    },
    panel: {
        flexGrow: 1,
        width: "100%",
    },
    heading: {
        fontSize: theme.typography.pxToRem(18),
    },
    details: {
        flexGrow: 1,
        flexDirection: "row" as "row",
        flexWrap: "wrap" as "wrap",
        // justifyContent: 'center',
        width: "100%",
        // height: "400px",
    },
    paper: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
})

interface ISummaryProps {
    classes: any; // added by material-ui withStyles css
    EnergyPerSource: any;
    TimePerSource: any;
    CostPerSource: any;
    CostPerEnergy: any;
    OpComplete: boolean;
}

const COLORS = [
    '#033F63',
    '#F5761A',
    '#79B791',
    '#F7EBEC',
];

const ITEMS = [
    'grid',
    'generator',
    'inverter',
    'solar'
];


class Summary extends Component<ISummaryProps> {
    constructor(props: ISummaryProps) {
        super(props)
    }

    render() {
        const { classes, EnergyPerSource, TimePerSource, CostPerSource, CostPerEnergy, OpComplete } = this.props;
        return (
            <div className={classes.container}>
                <ExpansionPanel className={classes.panel} defaultExpanded>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <div className={classes.column}>
                            <Typography className={classes.heading}>Summary</Typography>
                        </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails className={classes.details}>
                        <div className={classes.child}>
                            <PieDonut
                                Name={'Energy / Source Type'}
                                OpComplete={OpComplete}
                                Data={EnergyPerSource}
                            />
                        </div>
                        <div className={classes.child}>
                            <PieDonut
                                Name={'Time / Source Type'}
                                OpComplete={OpComplete}
                                Data={TimePerSource}
                            />
                        </div>
                        <div className={classes.child}>
                            <PieDonut
                                Name={'Cost / Source Type'}
                                OpComplete={OpComplete}
                                Data={CostPerSource}
                            />
                        </div>
                        <div className={classes.child}>
                            <PieDonut
                                Name={'Cost / Energy'}
                                OpComplete={OpComplete}
                                Data={CostPerEnergy}
                            />
                        </div>
                        <DiscreteColorLegend
                            colors={COLORS}
                            // @ts-ignore
                            onItemMouseEnter={i => this.setState({ hoveredItem: i })}
                            onItemMouseLeave={() => this.setState({ hoveredItem: false })}
                            orientation="horizontal"
                            width={300}
                            items={ITEMS}
                        />
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </div>
        );
    }
}

const mapStateToProps = (state: any) => {
    let analyticsState = getAnalyticsData(state)
    return {
        EnergyPerSource: analyticsState.EnergyPerSource,
        TimePerSource: analyticsState.TimePerSource,
        CostPerSource: analyticsState.CostPerSource,
        CostPerEnergy: analyticsState.CostPerEnergy,
        OpComplete: analyticsState.OpComplete,
        error: getAnalyticsDataError(state),
    }
};

export default connect(mapStateToProps)(withStyles(styles, { withTheme: true })(Summary));
