import React from 'react';
import Line from './Line';
import { withStyles } from '@material-ui/core';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import { getAnalyticsData, getAnalyticsDataError } from '../selectors/data';
import { connect } from 'react-redux';


const styles = (theme: any) => ({
	column: {
        flexGrow: 1,
		justifyContent: 'center',
	},
	heading: {
		fontSize: theme.typography.pxToRem(18),
	},
    panel: {
        width: "100%",
    },
    details: {
        flexGrow: 1,
        // flexDirection: "column" as "column",
        flexWrap: "wrap" as "wrap", 
        justifyContent: 'space-around',
        width: "100%",
    },
})

interface IDimensions {
    width: Number,
    height: Number 
}

interface IBatteriesProps {
    classes: any; // added by material-ui withStyles css
    BatteryCapData: any;
    BatteryDodData: any;
    OpComplete: boolean;
    error: any;
}

interface IBatteriesState {
    graph: IDimensions,
}


class Batteries extends React.Component<IBatteriesProps, IBatteriesState> {
    constructor(props: IBatteriesProps) {
        super(props)
        this.state = {
            graph: {
                width: 600,
                height: 300
            }
        }
    }

    render() {
        const { classes, BatteryCapData, BatteryDodData, OpComplete, error } = this.props;
        return (
            <ExpansionPanel className={classes.panel} defaultExpanded>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <div className={classes.column}>
                        <Typography className={classes.heading}>Batteries</Typography>
                    </div>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails className={classes.details}>
                    <Line
                        graph={this.state.graph}
                        xAxisName={'Time'}
                        yAxisName={'Battery Capacity'}
                        Data={BatteryCapData}
                        OpComplete={OpComplete}
                        error={error}
                    />
                    <Line 
                        graph={this.state.graph}
                        xAxisName={'Time'}
                        yAxisName={'Battery DOD'} 
                        Data={BatteryDodData}
                        OpComplete={OpComplete}
                        error={error}
                    />
                </ExpansionPanelDetails>
            </ExpansionPanel>
        );
    }
}
            

const mapStateToProps = (state: any) => {
    let analyticsState = getAnalyticsData(state)
    return {
        BatteryCapData: analyticsState.BatteryCapData,
        BatteryDodData: analyticsState.BatteryDodData,
        OpComplete: analyticsState.OpComplete,
        error: getAnalyticsDataError(state),
}};

export default connect(mapStateToProps)(withStyles(styles, { withTheme: true })(Batteries));
