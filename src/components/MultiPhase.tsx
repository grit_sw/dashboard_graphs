import React from 'react';
import { withStyles } from '@material-ui/core';
import Moment from 'moment';
import { format } from 'd3-format';
import color from '../colors/colors';
import 'react-vis/dist/style.css';

import {
    XYPlot,
    XAxis,
    YAxis,
    HorizontalGridLines,
    VerticalGridLines,
    LineSeries,
    ChartLabel
} from 'react-vis';
import Loading from './Loader';

const styles = () => ({
    container: {
        display: 'flex',
        justifyContent: 'center',
    },
})

interface IDimensions {
    width: Number,
    height: Number
}

interface IMultiPhaseProps {
    classes: any; // added by material-ui withStyles css
    // props: any; // added by material-ui withStyles css
    xAxisName: string;
    yAxisName: string;
    graph: IDimensions;
    PhaseOne: any;
    PhaseTwo: any;
    PhaseThree: any;
    OpComplete: boolean;
}


class MultiPhase extends React.Component<IMultiPhaseProps> {
    constructor(props: IMultiPhaseProps) {
        super(props)
    }

    render() {
        const { graph, classes, PhaseOne, PhaseTwo, PhaseThree, OpComplete } = this.props;
        if (OpComplete) {
            return (
                <div className={classes.container}>
                    <XYPlot
                        width={graph.width}
                        height={graph.height}
                        margin={{
                            right: 75,
                            left: 75,
                        }}
                    >
                        <HorizontalGridLines style={{ stroke: '#B7E9ED' }} />
                        <VerticalGridLines style={{ stroke: '#B7E9ED' }} />
                        <XAxis
                            // title={this.props.xAxisName}
                            xType="time"
                            // xDomain={[Moment().startOf('day'), Moment().endOf('day')]}
                            style={{
                                line: { stroke: '#ADDDE1' },
                                ticks: { stroke: '#ADDDE1' },
                                text: { stroke: 'none', fill: '#6b6b76', fontWeight: 600 }
                            }}
                        />
                        <YAxis
                            margin={100}
                            // title={this.props.yAxisName}
                        // tickFormat={(v: number) => `${format(".3s")(v)}`}
                        />
                        <ChartLabel
                            text={this.props.yAxisName}
                            className="alt-y-label"
                            includeMargin={false}
                            xPercent={-0.06}
                            yPercent={0.5}
                            style={{
                                transform: 'rotate(-90)',
                                textAnchor: 'end'
                            }}
                        />
                        <ChartLabel
                            text={this.props.xAxisName}
                            className="alt-x-label"
                            includeMargin={false}
                            xPercent={0.5}
                            yPercent={1.19}
                        />
                        <LineSeries
                            color={color.grid}
                            className="first-series"
                            data={PhaseOne}
                        />
                        <LineSeries
                            color={color.generator}
                            className="first-series"
                            data={PhaseTwo}
                        />
                        <LineSeries
                            color={color.inverter}
                            className="first-series"
                            data={PhaseThree}
                        />
                    </XYPlot>
                </div>
            );
        } else {
            return (
                <Loading
                    height={graph.height}
                    width={graph.width}
                />
            );
        }
    }
}

export default withStyles(styles, { withTheme: true })(MultiPhase);
