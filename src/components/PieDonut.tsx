import React from 'react';
import Pie from './Pie';
import Donut from './Donut';
import { withStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Loading from './Loader';

const HEIGHT = '300px';
const WIDTH = '300px';

const styles = (theme: any) => ({
    master: {
        display: 'flex',
        flexDirection: 'column' as 'column',
    },
    container: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row' as 'row',
        position: "relative" as "relative",
		height: HEIGHT,
		width: WIDTH,
    },
    outerPie: {
        // flexGrow: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        position: "absolute" as "absolute",
        top: 0,
        right: 0,
    },
	column: {
        flexGrow: 1,
		justifyContent: 'center',
	},
	heading: {
		fontSize: theme.typography.pxToRem(18),
	},
})

interface IHomeProps {
    classes: any; // added by material-ui withStyles css
    Data: any;
    OpComplete: boolean;
    Name: string;
}


class PieDonut extends React.Component<IHomeProps> {
    constructor(props: IHomeProps) {
        super(props)
    }

    render() {
        const { classes, Name, Data, OpComplete } = this.props;
        if (OpComplete) {
            return (
                <div className={classes.master}>
                <div><p>{Name}</p></div>
                <div className={classes.container}>
                    <div className={classes.outerPie}>
                        <Donut Data={ Data }/>
                    </div>
                    <div>
                        <Pie Data={ Data }/>
                    </div>
                </div>
                </div>
            );
        } else {
            return (
                <Loading 
                    height={HEIGHT}
                    width={WIDTH}
                />
            )
        }
    }
}
            

export default withStyles(styles, { withTheme: true })(PieDonut);
