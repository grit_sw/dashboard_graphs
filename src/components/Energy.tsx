import React from 'react';
import { withStyles, FormHelperText } from '@material-ui/core';
import StackedBar from './StackedBar';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import { getMonthlyData, getMonthlyDataError } from '../selectors/monthly';
import { connect } from 'react-redux';

const styles = (theme: any) => ({
	container: {
		display: 'flex',
		justifyContent: 'center',
        width: '100%',
	},
	heading: {
		fontSize: theme.typography.pxToRem(18),
	},
	column: {
        flexGrow: 1,
		justifyContent: 'center',
	},
	stacked_bar: {
		display: 'flex',
		justifyContent: 'center',
	}
})

interface IEnergyProps {
	classes: any; // added by material-ui withStyles css
}

interface IDimensions {
	width: number,
	height: number
}

interface IEnergyState {
	graph: IDimensions
}

class Energy extends React.Component<IEnergyProps, IEnergyState> {
	constructor(props: IEnergyProps) {
		super(props)
		this.state = {
			graph: {
				width: 1200,  // todo: set the value to be 100% of the parent container in integer.
				height: 300
			}
		}
	}

	render() {
		const { classes } = this.props;
		const style = this.state;
		return (
			<div className={classes.container}>
				<ExpansionPanel defaultExpanded>
					<ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
						<div className={classes.column}>
							<Typography className={classes.heading}>Energy</Typography>
						</div>
					</ExpansionPanelSummary>
					<ExpansionPanelDetails className={classes.stacked_bar}>
						<StackedBar
							graph={style.graph}
						/>
					</ExpansionPanelDetails>
				</ExpansionPanel>
			</div>
		);
	}
}
						
export default withStyles(styles, { withTheme: true })(Energy);
