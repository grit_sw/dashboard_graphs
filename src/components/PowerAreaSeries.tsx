import React, { Component } from 'react';
import Moment from 'moment';
import { format } from 'd3-format';
import { connect } from 'react-redux';
import { getDailyData, getDailyDataError } from '../selectors/daily';
import { ScaleLoader } from 'react-spinners';
import {
    XYPlot,
    XAxis,
    YAxis,
    VerticalGridLines,
    HorizontalGridLines,
    AreaSeries,
    Hint,
    MarkSeries,
    DiscreteColorLegend,
    ChartLabel
} from 'react-vis';
import color from '../colors/colors';
import Loading from './Loader';
// import DiscreteColorLegend from 'legends/discrete-color-legend';

interface IDimensions {
    width: number,
    height: number
}

interface PowerIAreaSeriesProps {
    graph: IDimensions
    DailyGridPower: Array<object>,
    DailyGeneratorPower: Array<object>,
    DailyInverterPower: Array<object>,
    DailySolarPower: Array<object>,
    OpComplete: Boolean,
}

interface PowerIAreaSeriesStates {
    value: any
    value2: any
    value3: any
    value4: any
    hoveredItem: any
}

const ITEMS = [
    'grid',
    'generator',
    'inverter',
    'solar'
];

const COLORS = [
    '#033F63',
    '#F5761A',
    '#79B791',
    '#F7EBEC',
];

class PowerAreaSeries extends Component<PowerIAreaSeriesProps, PowerIAreaSeriesStates> {
    constructor(props: PowerIAreaSeriesProps) {
        super(props);

        this.state = {
            value: null,
            value2: null,
            value3: null,
            value4: null,
            hoveredItem: false
        };
    }

    _forgetValue = () => {
        this.setState({
            value: null,
            value2: null,
            value3: null,
            value4: null,
            hoveredItem: false
        });
    };

    _rememberValue = (value: any) => {
        console.log(value)
        this.setState({ value });
    };

    _rememberValue2 = (value2: any) => {
        console.log(value2)
        this.setState({ value2 });
    };

    _rememberValue3 = (value3: any) => {
        console.log(value3)
        this.setState({ value3 });
    };

    _rememberValue4 = (value4: any) => {
        console.log(value4)
        this.setState({ value4 });
    };

    render() {
        const { value, value2, value3, value4, hoveredItem } = this.state;
        const { graph, DailyGridPower, DailyGeneratorPower, DailyInverterPower, DailySolarPower, OpComplete } = this.props;
        // if (!(Object.entries(DailyGridPower[0]).length === 0 && DailyGridPower[0].constructor === Object)) {
        if (OpComplete) {
            return (
                <XYPlot
                    width={graph.width}
                    height={graph.height}
                    margin={{ left: 75 }}
                >
                    {/* <VerticalGridLines /> */}
                    <HorizontalGridLines />
                    <XAxis
                        // title='Time'
                        xType="time"
                        // xDomain={[Moment().startOf('day'), Moment().endOf('day')]}
                        // style={{
                        //     text: { stroke: 'none', fill: '#6b6b76', fontWeight: 600 }
                        // }}
                    />
                    <YAxis
                        // left={}
                        // title='Power (W)'
                        tickFormat={(v: number) => `${format(".3s")(v)}W`}
                    />
                    <ChartLabel
                        text="Power (W)"
                        className="alt-y-label"
                        includeMargin={false}
                        xPercent={-0.06}
                        yPercent={0.5}
                        style={{
                            transform: 'rotate(-90)',
                            textAnchor: 'end'
                        }}
                    />
                    <ChartLabel
                        text="Time(Hours)"
                        className="alt-x-label"
                        includeMargin={false}
                        xPercent={0.5}
                        yPercent={1.19}
                    />
                    <AreaSeries
                        className="area-series-example"
                        curve="curveLinear"  // See https://github.com/d3/d3-shape#curves for options.
                        getNull={(d: any) => d.y !== null}
                        color={color.grid}
                        // onNearestX={this._rememberValue}
                        onNearestX={this._rememberValue}
                        // onSeriesMouseOut={this._forgetValue}
                        data={DailyGridPower}
                    />
                    <AreaSeries
                        className="area-series-example"
                        curve="curveLinear"  // See https://github.com/d3/d3-shape#curves for options.
                        getNull={(d: any) => d.y !== null}
                        data={DailyGeneratorPower}
                        color={color.generator}
                        onNearestX={this._rememberValue2}
                    />
                    <AreaSeries
                        className="area-series-example"
                        curve="curveLinear"  // See https://github.com/d3/d3-shape#curves for options.
                        getNull={(d: any) => d.y !== null}
                        data={DailyInverterPower}
                        onNearestX={this._rememberValue3}
                        color={color.inverter}
                    />
                    <AreaSeries
                        className="area-series-example"
                        curve="curveLinear"  // See https://github.com/d3/d3-shape#curves for options.
                        getNull={(d: any) => d.y !== null}
                        data={DailySolarPower}
                        onNearestX={this._rememberValue4}
                        color={color.solar}
                    />
                    <DiscreteColorLegend
                        colors={COLORS}
                        // @ts-ignore
                        onItemMouseEnter={i => this.setState({ hoveredItem: i })}
                        onItemMouseLeave={() => this.setState({ hoveredItem: false })}
                        orientation="horizontal"
                        width={300}
                        items={ITEMS}
                    />
                    {!value ?
                        null
                        : (
                            value.y === 0 || value.y === null ?
                                null
                                :
                                <Hint
                                    value={value}
                                    style={{
                                        fontSize: 14,
                                        text: {
                                            display: 'none'
                                        },
                                        value: {
                                            color: '#FFFFFF'
                                        }
                                    }}
                                >
                                    <div
                                        style={{
                                            fontSize: 14,
                                            width: '250px',
                                            backgroundColor: "#000000",
                                            // text: {
                                            //     display: 'none'
                                            // },
                                            color: '#FFFFFF'
                                        }}
                                    >
                                        {
                                            <div>
                                                <div>{`${value.x}`}</div>
                                                <div style={{ textAlign: "left" }}>Grid</div>
                                                <div style={{ textAlign: "left" }}>{`${Number((value.y).toFixed(3))}`}</div>
                                            </div>
                                        }
                                    </div>
                                </Hint>
                        )
                    }
                    {!value2 ?
                        null
                        : (
                            value2.y === 0 || value2.y === null ?
                                null
                                :
                                <Hint
                                    value={value2}
                                    style={{
                                        fontSize: 14,
                                        text: {
                                            display: 'none'
                                        },
                                        value: {
                                            color: 'red'
                                        }
                                    }}
                                >
                                    <div
                                        style={{
                                            fontSize: 14,
                                            width: '250px',
                                            backgroundColor: "#000000",
                                            // text: {
                                            //     display: 'none'
                                            // },
                                            color: '##FFFFFF'
                                        }}
                                    >
                                        {
                                            <div>
                                                <div>{`${value2.x}`}</div>
                                                <div style={{ textAlign: "left" }}>Generator</div>
                                                <div style={{ textAlign: "left" }}>{`${Number((value2.y).toFixed(3))}`}</div>
                                            </div>
                                        }
                                    </div>
                                </Hint>
                        )
                    }
                    {!value3 ?
                        null
                        : (
                            value3.y === 0 || value3.y === null ?
                                null
                                :
                                <Hint
                                    value={value3}
                                    style={{
                                        fontSize: 14,
                                        text: {
                                            display: 'none'
                                        },
                                        value: {
                                            color: 'red'
                                        }
                                    }}
                                >
                                    <div
                                        style={{
                                            fontSize: 14,
                                            width: '250px',
                                            backgroundColor: "#000000",
                                            // text: {
                                            //     display: 'none'
                                            // },
                                            color: '#FFFFFF'
                                        }}
                                    >
                                        {
                                            <div>
                                                <div>{`${value3.x}`}</div>
                                                <div style={{ textAlign: "left" }}>Inverter</div>
                                                <div style={{ textAlign: "left" }}>{`${Number((value3.y).toFixed(3))}`}</div>
                                            </div>
                                        }
                                    </div>
                                </Hint>
                        )
                    }
                    {!value4 ?
                        null
                        : (
                            value4.y === 0 || value4.y === null ?
                                null
                                :
                                <Hint
                                    value={value4}
                                    style={{
                                        fontSize: 14,
                                        text: {
                                            display: 'none'
                                        },
                                        value: {
                                            color: 'red'
                                        }
                                    }}
                                >
                                    <div
                                        style={{
                                            fontSize: 14,
                                            width: '250px',
                                            backgroundColor: "#000000",
                                            // text: {
                                            //     display: 'none'
                                            // },
                                            color: 'red'
                                        }}
                                    >
                                        {
                                            <div>
                                                <div>{`${value4.x}`}</div>
                                                <div style={{ textAlign: "left" }}>Solar</div>
                                                <div style={{ textAlign: "left" }}>{`${Number((value4.y).toFixed(3))}`}</div>
                                            </div>
                                        }
                                    </div>
                                </Hint>
                        )
                    }
                </XYPlot>
            );
        } else {
            return (
                <Loading
                    height={`${graph.height}px`}
                    width={`${graph.width}px`}
                />
            )
        }
    }
}

const mapStateToProps = (state: any) => {
    let dailyData = getDailyData(state)
    return {
        DailyGridPower: dailyData.DailyGridPower,
        DailyGeneratorPower: dailyData.DailyGeneratorPower,
        DailyInverterPower: dailyData.DailyInverterPower,
        DailySolarPower: dailyData.DailySolarPower,
        OpComplete: dailyData.OpComplete,
        error: getDailyDataError(state),
    }
};

export default connect(mapStateToProps)(PowerAreaSeries);
