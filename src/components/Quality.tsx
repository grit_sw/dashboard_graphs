import React from 'react';
import  MultiPhase from './MultiPhase';
import { withStyles, AppBar, Tabs, Tab } from '@material-ui/core';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import { getRecentData, getRecentDataError } from '../selectors/recent';
import { connect } from 'react-redux';


const styles = (theme: any) => ({
    container: {
        display: 'flex',
        justifyContent: 'center',
        width: '100%',
    },
	column: {
        flexGrow: 1,
		justifyContent: 'center',
	},
	heading: {
		fontSize: theme.typography.pxToRem(18),
	},
    appBar: {
        // flexGrow: 1,
        // width: "100%",
        position: "relative" as "relative",
        top: 0,
        right: 0,
    },
    details: {
        flexGrow: 1,
        flexDirection: "column" as "column",
        // alignItems: 'center',
        // width: "100%",
    },
    panel: {
        width: "100%",
    },
})

interface IQualityProps {
    classes: any; // added by material-ui withStyles css
    CurrentPhaseOne: any;
    CurrentPhaseTwo: any;
    CurrentPhaseThree: any;
    VoltagePhaseOne: any;
    VoltagePhaseTwo: any;
    VoltagePhaseThree: any;
    PowerFactorOne: any;
    PowerFactorTwo: any;
    PowerFactorThree: any;
    OpComplete: boolean;
}

interface IDimensions {
    width: Number,
    height: Number 
}

interface IQualityState {
    tabValue: Number,
    graph: IDimensions,
}


class Quality extends React.Component<IQualityProps, IQualityState> {
    constructor(props: IQualityProps) {
        super(props)
        this.state = {
            tabValue: 0,
            graph: {
                width: 1200,
                height: 300
            }
        }
    }

    handleChange = (event: React.ChangeEvent<{}>, value: Number) => {
        this.setState({ tabValue: value });
      };

    render() {
        const { classes, CurrentPhaseOne, CurrentPhaseTwo, CurrentPhaseThree,
            VoltagePhaseOne, VoltagePhaseTwo, VoltagePhaseThree,
            PowerFactorOne, PowerFactorTwo, PowerFactorThree,
            OpComplete,
        } = this.props;
        const { tabValue } = this.state;
        return (
            <div className={classes.container}>
                <ExpansionPanel className={classes.panel} defaultExpanded>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <div className={classes.column}>
                            <Typography className={classes.heading}>Quality</Typography>
                        </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails className={classes.details}>
                        <AppBar className={classes.appBar}>
                        <Tabs value={tabValue} onChange={this.handleChange}>
                            <Tab label="Current" />
                            <Tab label="Voltage" />
                            <Tab label="Power Factor" />
                        </Tabs>
                        </AppBar>
                        {tabValue === 0 && <div><MultiPhase OpComplete={OpComplete} PhaseOne={CurrentPhaseOne} PhaseTwo={CurrentPhaseTwo} PhaseThree={CurrentPhaseThree} graph={this.state.graph} xAxisName={'Time'} yAxisName={'Current (A)'} /></div>}
                        {tabValue === 1 && <div><MultiPhase OpComplete={OpComplete} PhaseOne={VoltagePhaseOne} PhaseTwo={VoltagePhaseTwo} PhaseThree={VoltagePhaseThree} graph={this.state.graph} xAxisName={'Time'} yAxisName={'Voltage (V)'} /></div>}
                        {tabValue === 2 && <div><MultiPhase OpComplete={OpComplete} PhaseOne={PowerFactorOne} PhaseTwo={PowerFactorTwo} PhaseThree={PowerFactorThree} graph={this.state.graph} xAxisName={'Time'} yAxisName={'Power Factor'} /></div>}
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </div>
        );
    }
}


const mapStateToProps = (state: any) => {
    let recentDataState = getRecentData(state)
    return {
        CurrentPhaseOne: recentDataState.CurrentPhaseOne,
        CurrentPhaseTwo: recentDataState.CurrentPhaseTwo,
        CurrentPhaseThree: recentDataState.CurrentPhaseThree,
        VoltagePhaseOne: recentDataState.VoltagePhaseOne,
        VoltagePhaseTwo: recentDataState.VoltagePhaseTwo,
        VoltagePhaseThree: recentDataState.VoltagePhaseThree,
        PowerFactorOne: recentDataState.PowerFactorOne,
        PowerFactorTwo: recentDataState.PowerFactorTwo,
        PowerFactorThree: recentDataState.PowerFactorThree,
        OpComplete: recentDataState.OpComplete,
        error: getRecentDataError(state),
}};

export default connect(mapStateToProps)(withStyles(styles, { withTheme: true })(Quality));
