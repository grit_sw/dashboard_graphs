import React from 'react';
import { ScaleLoader } from "react-spinners";

const Loading = (props: any) => {
    return (
        <div style={{height: props.height, width: props.width}}>
        <ScaleLoader
            height={35}
            width={5}
            radius={5}
            margin={'5px'}
            color={'#375187'}
        />
        </div>
    );
}

export default Loading;