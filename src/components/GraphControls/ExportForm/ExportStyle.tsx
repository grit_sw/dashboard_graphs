import blueGrey from "@material-ui/core/colors/blueGrey"

const styles = (theme: any) => ({
    container: {
        display: 'flex',
        flexDirection: 'row' as 'row',
        // flexWrap: 'wrap' as "wrap",
        alignItems: 'center',
        marginTop: theme.spacing.unit * 10,
        // justifyContent: 'center',
        // height: '100vh',
        width: '100%'
    },
	textField: {
		marginLeft: '0',
		marginRight: '0',
		width: '100%',
	},
    textFieldHeight: {
        height: 8,
    },
    dense: {
        marginTop: 16,
    },
    menu: {
        width: 200,
    },
    app_item: {
        display: 'flex',
        flexDirection: 'row' as 'row',
        // width: '100%',
        flexGrow: 1,
        marginTop: '20%',
        marginBottom: '20%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    form_item: {
        display: 'flex',
        flexGrow: 1,
        flexDirection: 'column' as 'column',
        width: '75%',
        margin: 'auto',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    advert: {
        width: '10%',
        marginLeft: theme.spacing.unit * 10,
        // border: '1px solid black',
    },
	formLabel: {
		display: 'flex',
		color: blueGrey[900],
		flexDirection: "column" as "column",
		justifyContent: 'center',
		marginLeft: '0',
		marginRight: theme.spacing.unit,
		width: '100%',
	},
    formGroup: {
        width: '100%',
    },
    button:{
        marginTop:'25px',
    },
})


export default styles;
