export interface FormValues {
	start_time: string;
	stop_time: string;
	interval: string;
	interval_time_unit: string;
}

export interface IExportFormInterface {
	start_time: string;
	stop_time: string;
	interval: string;
	interval_time_unit: string;
}

export interface FormProps {
	start_time?: any;
	stop_time?: any;
	interval?: string;
	interval_time_unit?: string;
	handleData?: Function;
	handleDisplay: Function;
	handleModalToggle?: Function;
	display?: String;
	index?: Number;
	data?: any;
	classes: any; // added by material-ui withStyles css
	theme: any; // added by material-ui withStyles css
}

export interface LooseObject {
	[key: string]: any;
}


export interface ExportContainerState {
	display: string;
	data: Array<Object>;
}

export interface ExportContainerProps {
	index: Number;
	handleModalToggle: Function;
}


