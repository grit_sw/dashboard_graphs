import React from 'react';
import { InjectedFormikProps } from 'formik';
import FormGroup from '@material-ui/core/FormGroup';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import { TextField, Button } from '@material-ui/core';
import { CSVLink, CSVDownload } from "react-csv";
import {FormProps, FormValues} from './ExportInterface';
import Spinner from "../../../components/Spinner";
import DatePicker from "../../../components/DatePicker";

const ExportForm: React.FC<InjectedFormikProps<FormProps, FormValues>> = (
	props,
  ) => {
	  if(props.display === "loading") {
		  return (
			<div className={props.classes.app_item}>
				<div className={props.classes.form_item}>
					<Spinner />
				</div>
			</div>
		  )
	  } else if(props.display === "data") {
		  return (
			<CSVLink className={props.classes.app_item} data={props.data}>Download me</CSVLink>
		  )
	  } else {
		  return (
		   <div className={props.classes.app_item}>
			   <form onSubmit={props.handleSubmit} noValidate autoComplete="off">
				   <div className={props.classes.form_item}>
					   <FormGroup row={true} className={props.classes.formGroup}>
						   <InputLabel className={props.classes.formLabel} htmlFor="start_time">
							   <Typography variant="subtitle1" color="inherit" noWrap>
								   Start Time
							   </Typography>
						   </InputLabel>
						   <DatePicker
							   id="start-time"
							   type="datetime-local"
							   className={props.classes.textField}
							   style={{width: '100%'}}
							   helperText={props.touched.start_time ? props.errors.start_time : ""}
							   error={props.touched.start_time && Boolean(props.errors.start_time)}
							   onChange={(e : any) => {
								   console.log(e)
								   props.handleChange(e);
								   props.setFieldValue('start_time', e)
								}}
							   value={props.values.start_time}
							   margin="normal"
							   // variant="outlined"
							   InputProps={{ 
								   classes: { 
									   input: props.classes.textFieldHeight,
									   disabled: props.classes.disabledField,
								   } 
							   }}
						   />
					   </FormGroup>
					   <FormGroup row={true}>
						   <InputLabel className={props.classes.formLabel} htmlFor="stop-time">
							   <Typography variant="subtitle1" color="inherit" noWrap>
								   Stop Time
							   </Typography>
						   </InputLabel>
						   <DatePicker
							   id="stop-time"
							   type="datetime-local"
							   helperText={props.touched.stop_time ? props.errors.stop_time : ""}
							   error={props.touched.stop_time && Boolean(props.errors.stop_time)}
							   className={props.classes.textField}
							   onChange={(e : any) => {
									console.log(e)
									props.handleChange(e);
									props.setFieldValue('stop_time', e)
							 	}}
							   value={props.values.stop_time}
							   margin="normal"
							   // variant="outlined"
							   InputProps={{ 
								   classes: { 
									   input: props.classes.textFieldHeight,
									   disabled: props.classes.disabledField,
								   } 
							   }}
						   />
					   </FormGroup>
					   <FormGroup row={true} className = {props.classes.formGroup}>
						   <InputLabel className={props.classes.formLabel} htmlFor="interval">
							   <Typography variant="subtitle1" color="inherit" noWrap>
								   Interval
							   </Typography>
						   </InputLabel>
						   <TextField
							   id="interval"
							   type='number'
							   helperText={props.touched.interval ? props.errors.interval : ""}
							   error={props.touched.interval && Boolean(props.errors.interval)}
							   className={props.classes.textField}
							   onChange={props.handleChange('interval')}
							   value={props.values.interval}
							   margin="normal"
							   // variant="outlined"
							   InputProps={{ 
								   classes: { 
									   input: props.classes.textFieldHeight,
									   disabled: props.classes.disabledField,
								   } 
							   }}
						   />
					   </FormGroup>
					   <FormGroup row={true} className = {props.classes.formGroup}>
						   <InputLabel className={props.classes.formLabel} htmlFor="interval-time-unit">
							   <Typography variant="subtitle1" color="inherit" noWrap>
								   Interval Time Unit
							   </Typography>
						   </InputLabel>
						   <TextField
							   id="interval-time-unit"
							   name="interval-time-unit"
							   select
							   className={props.classes.textField}
							   helperText={props.touched.interval_time_unit ? props.errors.interval_time_unit : ""}
							   error={props.touched.interval_time_unit && Boolean(props.errors.interval_time_unit)}
							   value={props.values.interval_time_unit ? props.values.interval_time_unit: 'Day'}
							   onChange={e => {
								   // @ts-ignore
								   props.handleChange(e)
								   props.setFieldValue('interval_time_unit', e.target.value)
							   }}
							   SelectProps={{
								   MenuProps: {
									   className: props.classes.menu,
								   },
							   }}
							   margin="normal"
						   >
							   {
								   ['Day', 'Week', 'Month'].map((item: any, index: string | number | undefined) => (
									   <MenuItem value={item as any} key={index}>{item}</MenuItem>)
								   )
							   }
						   </TextField>
					   </FormGroup>
					   <Button
						   className={props.classes.button}
						   disabled={props.isSubmitting}
						   type="submit"
						   color="primary"
						   variant="contained"
						   onClick={() => console.log(props.display)}
					   >
						   Export
					   </Button>
				   </div>
			   </form>
			   </div>
	   );
	  }
  }
  
export default ExportForm;
