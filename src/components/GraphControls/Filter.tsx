import { withFormik } from 'formik';
import FilterForm from './Filter/FilterForm';
import {FormProps, FormValues} from './Filter/FilterInterface';
import FilterSchema from './Filter/FilterSchema';
import { withStyles } from '@material-ui/core';
import styles from './Filter/FilterStyle';
import FilterRequest from '../../api/filter';


const Filter = withFormik<FormProps, FormValues>({
	mapPropsToValues: (props) => ({ 
        user_email: `${localStorage.getItem('email')}`,
        configuration_name: "",
        configurations: [],
        handleModalToggle: props.handleModalToggle,
        index: props.index
    }),
	validationSchema: FilterSchema,
	handleSubmit: (values: any, { setSubmitting, setErrors }: any) => {
		console.log(values)
		let body = {
            email: values.email,
            password: values.password
		}
		console.log(body)
        FilterRequest(body)
            .then((response: any) => {
                console.log(response);
                // let data = response.data.data;
                if (Boolean(response.data)) {
                    console.log(response.data);
                }
            })
            .catch((err: any) => {
                setSubmitting(false);  // ensures that the button is reenabled
                console.log(err);
                console.log(err.response);

            if (err.response.status === 400) {
                console.log('Filter Unsuccessful');
                console.log(err.response.data)
                console.log(err.response.data.message)
                setErrors({'email': err.response.data.message})
            }
        });
    }
})(FilterForm);

export default withStyles(styles, { withTheme: true })(Filter);
