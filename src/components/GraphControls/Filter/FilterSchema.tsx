import * as Yup from 'yup';

const ExportFormSchema = Yup.object().shape({
	start_time: Yup.date()
		.required('Please select a start time.'),
    stop_time: Yup.date()
    .required('Please select a stop time.'),
	interval: Yup.string()
		.required('Required.'),
	interval_time_unit: Yup.string()
		.required('Required.'),
	},
)

export default ExportFormSchema;
