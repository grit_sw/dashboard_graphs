export interface FormValues {
	user_email: string;
	configuration_name: string;
}

export interface IExportFormInterface {
	user_email: string;
	configuration_name: string;
}

export interface FormProps {
	user_email: string;
	configuration_name: string;
	configurations: any;
	classes: any; // added by material-ui withStyles css
	theme: any; // added by material-ui withStyles css
	handleModalToggle: Function;
	index: Number;
}

export interface FormProps1 {
	user_email: string;
	configuration_name: string;
	configurations: any,
	classes: any; // added by material-ui withStyles css
	theme: any; // added by material-ui withStyles css
	onFetchConfig: Function,
	handleModalToggle: Function,
	index: Number,
}


