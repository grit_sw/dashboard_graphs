const styles = (theme: any) => ({
    container: {
        display: 'flex',
        flexDirection: 'row' as 'row',
        // flexWrap: 'wrap' as "wrap",
        alignItems: 'center',
        marginTop: theme.spacing.unit * 10,
        // justifyContent: 'center',
        // height: '100vh',
        width: '100%'
    },
	textField: {
		marginLeft: theme.spacing.unit,
		marginRight: theme.spacing.unit,
		width: '40%',
	},
    textFieldHeight: {
        height: 8,
    },
    dense: {
        marginTop: 16,
    },
    menu: {
        width: 200,
    },
    app_item: {
        display: 'flex',
        flexDirection: 'row' as 'row',
        width: '100%',
        // flexGrow: 1,
        // marginTop: '20%',
        // marginBottom: '20%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    formRow: {
        // display: 'flex',
        // flexDirection: 'row' as 'row',
        width: '100%',
        // flexGrow: 1,
        // marginTop: '20%',
        // marginBottom: '20%',
        // height: '100%',
        // alignItems: 'center',
        // justifyContent: 'center',
    },
    form_item: {
        display: 'flex',
        flexGrow: 1,
        flexDirection: 'column' as 'column',
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
	formLabel: {
		display: 'flex',
	    flexDirection: "column" as "column",
		justifyContent: 'center',
		marginLeft: theme.spacing.unit,
		marginRight: theme.spacing.unit,
		width: '40%',
	},
})


export default styles;
