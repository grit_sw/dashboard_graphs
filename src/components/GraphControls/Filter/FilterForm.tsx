import React from 'react';
import { InjectedFormikProps } from 'formik';
import FormGroup from '@material-ui/core/FormGroup';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import { TextField, Button } from '@material-ui/core';
import {FormProps1, FormValues} from './FilterInterface';
import { doSaveConfig } from '../../../actions/global';
import { connect } from 'react-redux';



const FilterForm: React.FC<InjectedFormikProps<FormProps1, FormValues>> = (
	props,
  ) => (
	<div className={props.classes.app_item}>
		<form onSubmit={props.handleSubmit} noValidate autoComplete="off" className={props.classes.formRow}>
			<div className={props.classes.form_item}>
                <FormGroup row={true} className={props.classes.formRow}>
                    <InputLabel className={props.classes.formLabel} htmlFor="user-email">
                        <Typography variant="subtitle1" color="inherit" noWrap>
                            User Email
                        </Typography>
                    </InputLabel>
                    <TextField
                        id="user-email"
                        name="user-email"
                        type="email"
                        // select
                        className={props.classes.textField}
                        helperText={props.touched.user_email ? props.errors.user_email : ""}
                        error={props.touched.user_email && Boolean(props.errors.user_email)}
                        value={props.values.user_email}
                        onChange={e => {
                            // @ts-ignore
                            props.handleChange(e)
                            props.setFieldValue('user_email', e.target.value)
                        }}
                        SelectProps={{
                            MenuProps: {
                                className: props.classes.menu,
                            },
                        }}
                        margin="normal"
                    />
                        {/* {
                            ['Day', 'Week', 'Month'].map((item: any, index: string | number | undefined) => (
                                <MenuItem value={item as any} key={index}>{item}</MenuItem>)
                            )
                        }
                    </TextField> */}
                </FormGroup>
                <FormGroup row={true} className={props.classes.formRow}>
                    <InputLabel className={props.classes.formLabel} htmlFor="configuration-name">
                        <Typography variant="subtitle1" color="inherit" noWrap>
                            Configurations
                        </Typography>
                    </InputLabel>
                    <TextField
                        id="configuration-name"
                        name="configuration-name"
                        select
                        className={props.classes.textField}
                        helperText={props.touched.configuration_name ? props.errors.configuration_name : ""}
                        error={props.touched.configuration_name && Boolean(props.errors.configuration_name)}
                        value={props.values.configuration_name}
                        onChange={e => {
                            props.handleChange(e)
                            let configObj = e.target.value
                            // @ts-ignore
                            props.setFieldValue('configuration_name', configObj)
                            props.onFetchConfig({
                                // @ts-ignore
                                id: configObj.configuration_id,
                                // @ts-ignore
                                name: configObj.configuration_name
                            })
                        }}
                        SelectProps={{
                            MenuProps: {
                                className: props.classes.menu,
                            },
                        }}
                        margin="normal"
                    >
                        {
                            props.configurations.map((item: any, index: string | number | undefined) => (
                                <MenuItem value={item as any} key={index}>{item.configuration_name}</MenuItem>)
                            )
                        }
                    </TextField>
                </FormGroup>
				<Button
                    disabled={props.isSubmitting}
                    type="submit"
                    color="primary"
                    variant="contained"
                    onClick={() => props.handleModalToggle(props.index)}
                >
                    View
                </Button>
			</div>
		</form>
		</div>
);

const mapDispatchToProps = (dispatch: any) => ({
    onFetchConfig: (configID: any) => {
       dispatch(doSaveConfig(configID))
    }
})
  
export default connect(null, mapDispatchToProps)(FilterForm);
