import { withFormik } from 'formik';
import { connect } from 'react-redux';
import LoginForm from './LoginForm/LoginForm';
import {FormProps, FormValues} from './LoginForm/LoginInterface';
import LoginSchema from './LoginForm/LoginSchema';
import { withStyles } from '@material-ui/core';
import styles from './LoginForm/LoginStyle';
import LoginUser from '../../api/login';
import userConfig from '../../api/config';
import { doSaveConfig } from '../../actions/global';
import { doChangeAuth } from '../../actions/auth';
import { doSaveUserDetails } from '../../actions/user';
import { doFetchConfig } from '../../actions/global';
import store from '../../store'
// import { connect } from 'http2';


const Login = withFormik<FormProps, FormValues>({
	mapPropsToValues: (props) => ({ 
		email: '',
        password: '',
        handleModalToggle: props.handleModalToggle,
        index: props.index
	}),
	validationSchema: LoginSchema,
	handleSubmit: (values: any, { props, setSubmitting, setErrors }: any) => {
		console.log(values)
		let body = {
            user_id: values.email,
            password: values.password
		}
		console.log(body)
        LoginUser(body)
            .then((response: any) => {
                console.log(response);
                let data = response.data.data;
                let token = response.data.token;
                let full_name = response.data.data.full_name;
                let email = response.data.data.email;
                if (response.data.success) {
                    if (response.status === 201) {
                        console.log('Successful login');
                        console.log('Login Successful');
                        if (data && token && data.role_id) {
                            const role_id = data.role_id;
                            console.log('Storing token, new');
                            localStorage.setItem('token', token);
                            localStorage.setItem('full_name', full_name);
                            localStorage.setItem('role_id', role_id);
                            localStorage.setItem('email', email);
                            console.log(store.getState().authState)
                            console.log(localStorage.getItem('token'))
                            const userObj = {
                                name: data.full_name
                            }
                            console.log(userObj)
                            props.onFetchUser(userObj)
                            props.onFetchConfig('me')
                            // console.log(props.handleModalToggle)
                            props.handleModalToggle(props.index)
                            
                            props.onUpdateAuth(true)
                            location.reload()
                            props.onUpdateAuth(true)
                            // userConfig()
                            // .then((data: any) => {
                            //     console.log(data)
                            //     let {data: {data: configData}} = data
                            //     console.log(configData);
                            //     let selectedConfig = {
                            //         name :configData[0].configuration_name,
                            //         id: configData[0].configuration_id
                            //     }
                            
                            //     props.onFetchConfig(selectedConfig)
                            //     
                            //     // console.log(store.getState().globalState.config)
                            // })
                        }
                    }
                }
            })
            .catch((err: any) => {
                setSubmitting(false);  // ensures that the button is reenabled
                console.log(err);
                console.log(err.response);
                if(err.response) {
                    setErrors({'email': err.response.data.message})
                }
            if (err.response.status === 400) {
                console.log('Login Unsuccessful');
                console.log(err.response.data)
                console.log(err.response.data.message)
                setErrors({'email': err.response.data.message})
            }
        });
    }
})(LoginForm);

const mapDispatchToProps = (dispatch: any) => ({
    onFetchConfig: (type : string)=> {
        dispatch(doFetchConfig(type))
    },
    onUpdateAuth: (auth: boolean) => {
        dispatch(doChangeAuth(auth))
    },
    onFetchUser: (userObj: any) => {
        dispatch(doSaveUserDetails(userObj))
    }
})



export default connect(null, mapDispatchToProps)(withStyles(styles, { withTheme: true })(Login));
