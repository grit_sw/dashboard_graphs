import { withFormik } from 'formik';
import ExportForm from './ExportForm/ExportForm';
import {FormProps, FormValues, LooseObject} from './ExportForm/ExportInterface';
import ExportSchema from './ExportForm/ExportSchema';
import { withStyles } from '@material-ui/core';
import styles from './ExportForm/ExportStyle';
import { connect } from 'react-redux';
import { getConfig } from '../../selectors/global';
import ExportRequest from '../../api/export';
import Spinner from '../../components/Spinner';
import Moment from 'moment';

// let bool = true;

function toHHMMSS(number: any) {
    let sec_num: any = parseInt(number, 10);
    let hours: any  = Math.floor(sec_num / 3600);
    let minutes: any = Math.floor((sec_num - (hours * 3600)) / 60);
    let seconds: any = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    return hours+':'+minutes+':'+seconds;
}

function roundTo(num: any) {
    // @ts-ignore
    return +(Math.round(num + "e+2") + "e-2");
}


const Export = withFormik<FormProps, FormValues>({
	mapPropsToValues: () => ({ 
        start_time: "05-01-2019 11:00",
        stop_time: "05-05-2019 11:00",
        interval: "",
        interval_time_unit: "",
        display: "",
        data: [],
        handleDisplay: ""
    }),
	validationSchema: ExportSchema,
	handleSubmit: (values: any, { props, setSubmitting, setErrors }: any) => {

        console.log(values);
        console.log(props.handleModalToggle)
        props.handleModalToggle(props.index);
        props.handleDisplay('loading')
        // const configId = props.config.id
        const configId = 'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF';
        const exportStartTime = values.start_time + ':00+01:00';
        const exportStopTime = values.stop_time + ':00+01:00';
        const interval = values.interval;
        const intervalUnit = values.interval_time_unit;
        console.log(exportStartTime, exportStopTime)
        props.handleModalToggle(props.index);
        ExportRequest(configId, exportStartTime, exportStopTime, interval, intervalUnit)
            .then((response: any) => {
                console.log(response);
                if (Boolean(response.data)) {
                    console.log(response.data);
                }
                const data = response.data
                const dataArray: Array<Object> = [];
                const exportBuckets = data.aggregations.Power_.histogram.buckets;
                const dateInterval = {"unit":intervalUnit.toLowerCase(),"value":parseInt(interval)};
                console.log(dateInterval);
                exportBuckets.forEach((nest: any) => {
                    let energy: LooseObject = {};
                    if (nest.topHits.hits.hits.length >= 1) {
                        let energyToday = nest.topHits.hits.hits[0]._source.energyTodaySource;
                        energyToday.forEach((energyData: LooseObject) =>{
                            energy[energyData.sourceName.toLowerCase()] = energyData.energyToday;
                        });
                    }
                    const date = nest.key;
                    nest.powerNest.configID.buckets.forEach((config: any) => {
                        const configID = config.key;
                        config.SourceName.buckets.forEach((source: LooseObject) => {
                            var sourceName = source.key;
                            var time = toHHMMSS(source.doc_count*5);
                            var power = source.powerStats;
                            dataArray.push(
                                {
                                    "Start Date": Moment(date).format("YYYY-MM-DD HH:mm:ss"),
                                    "Stop Date": Moment(date).add(dateInterval.value,dateInterval.unit).format("YYYY-MM-DD HH:mm:ss"),
                                    "Source Name":sourceName,
                                    "Energy (Wh)":roundTo(energy[sourceName.toLowerCase()] || 0),
                                    "Time (hh:mm:ss)":time,
                                    "Avg Power(W)": roundTo(power.avg),
                                    "Min Power(W)" : roundTo(power.min),
                                    "Max Power(W)":roundTo(power.max)
                                }
                            );
                        });
                    });
                });
                console.log(dataArray)
                props.handleDisplay('data')
                props.handleData(dataArray)
            })
            .catch((err: any) => {
                setSubmitting(false);  // ensures that the button is reenabled
                console.log(err);
                console.log(err.message);

            // if (err.response.status === 400) {
            //     console.log('Export Unsuccessful');
            //     console.log(err.response.data)
            //     console.log(err.response.data.message)
            //     setErrors({'email': err.response.data.message})
            // }
        });
    }
})(ExportForm);

const mapStateToProps = (state : any) => ({
	config: getConfig(state)
})

export default connect(mapStateToProps)(withStyles(styles, { withTheme: true })(Export));
