import React from 'react';
import { InjectedFormikProps } from 'formik';
import FormGroup from '@material-ui/core/FormGroup';
import InputLabel from '@material-ui/core/InputLabel';
import Typography from '@material-ui/core/Typography';
import { TextField, Button } from '@material-ui/core';
import Toolbar from '@material-ui/core/Toolbar';
import {FormProps, FormValues} from './LoginInterface';

const LoginForm: React.FC<InjectedFormikProps<FormProps, FormValues>> = (
	props,
  ) => (
	<div className={props.classes.app_item}>
		<form onSubmit={props.handleSubmit} noValidate autoComplete="off">
			<div className={props.classes.form_item}>
				<FormGroup row={true} className = {props.classes.formGroup}>
					<InputLabel className={props.classes.formLabel} htmlFor="email">
						<Typography variant="subtitle1" color="inherit" noWrap>
							User Email
						</Typography>
					</InputLabel>
					<TextField
						id="email"
						className={props.classes.textField}
						helperText={props.touched.email ? props.errors.email : ""}
						error={props.touched.email && Boolean(props.errors.email)}
						onChange={props.handleChange('email')}
						value={props.values.email}
						// margin="normal"
						// variant="outlined"
						InputProps={{ 
							classes: { 
								input: props.classes.textFieldHeight,
								disabled: props.classes.disabledField,
							} 
						}}
					/>
				</FormGroup>
				<FormGroup row={true}>
					<InputLabel className={props.classes.formLabel} htmlFor="password">
						<Typography variant="subtitle1" color="inherit" noWrap>
							Password
						</Typography>
					</InputLabel>
					<TextField
						id="password"
						type="password"
						helperText={props.touched.password ? props.errors.password : ""}
						error={props.touched.password && Boolean(props.errors.password)}
						className={props.classes.textField}
						onChange={props.handleChange('password')}
						value={props.values.password}
						// margin="normal"
						// variant="outlined"
						InputProps={{ 
							classes: { 
								input: props.classes.textFieldHeight,
								disabled: props.classes.disabledField,
							} 
						}}
					/>
				</FormGroup>
				<Button
				className={props.classes.button}
				disabled={props.isSubmitting}
				type="submit"
				color="primary"
				variant="contained"
			>
				Login
			</Button>
			</div>
		</form>
		</div>
);
  
export default LoginForm;
