export interface FormValues {
	email: string;
	password: string;
}

export interface ILoginState {
    email: string,
    password: string,
    submit_disabled: boolean,
    error_message: string
}

export interface FormProps {
	email?: string;
	password?: string;
	handleModalToggle?: Function;
	index?: Function;
	classes: any; // added by material-ui withStyles css
	theme: any; // added by material-ui withStyles css
}
