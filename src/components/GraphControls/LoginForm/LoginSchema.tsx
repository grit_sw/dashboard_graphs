import * as Yup from 'yup';

const LoginSchema = Yup.object().shape({
	email: Yup.string()
		.email('Please enter a valid email address')
		.required('Please enter your email.'),
	password: Yup.string()
		.required('Please enter your password.'),
	},
)

export default LoginSchema;