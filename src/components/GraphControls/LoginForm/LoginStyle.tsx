import blueGrey from "@material-ui/core/colors/blueGrey"

const styles = (theme: any) => ({
    container: {
        display: 'flex',
        flexDirection: 'row' as 'row',
        // flexWrap: 'wrap' as "wrap",
        alignItems: 'center',
        marginTop: theme.spacing.unit * 10,
        // justifyContent: 'center',
        // height: '100vh',
        width: '100%'
    },
	textField: {
		marginLeft: theme.spacing.unit,
		marginRight: theme.spacing.unit,
		width: '55%',
	},
    textFieldHeight: {
        height: 8,
    },
    dense: {
        marginTop: 16,
    },
    menu: {
        width: 200,
    },
    app_item: {
        display: 'flex',
        flexDirection: 'row' as 'row',
        // width: '25%',
        flexGrow: 1,
        marginTop: '20%',
        marginBottom: '20%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    form_item: {
        display: 'flex',
        flexGrow: 1,
        flexDirection: 'column' as 'column',
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    advert: {
        width: '10%',
        marginLeft: theme.spacing.unit * 10,
        // border: '1px solid black',
    },
	formLabel: {
		display: 'flex',
		color: blueGrey[900],
		flexDirection: "column" as "column",
		justifyContent: 'center',
		marginLeft: theme.spacing.unit,
		marginRight: theme.spacing.unit,
		width: '30%',
	},
    button: {
        marginTop: '25px',
    },
    formGroup: {
        marginBottom: '20px',
    },
})


export default styles;
