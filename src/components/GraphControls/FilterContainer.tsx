import React, { Component } from 'react';
import Filter from '../../components/GraphControls/Filter';
import userConfig from '../../api/config';

interface FilterProps {
    handleModalToggle: Function,
    index: Number
}

interface FilterState {
    configurations: Array<Object>,
    user_email: string,
    configuration_name: string
}



class FilterContainer extends Component<FilterProps, FilterState>{
    constructor(props: FilterProps) {
        super(props);
        this.state = {
            configurations: [],
            user_email: "",
            configuration_name: ""
        }
    }

    componentDidMount() {
        userConfig('me')
        .then((data: any) => {
            let configurations = data.data.data
            let email = localStorage.getItem('email')
            console.log(configurations)
            this.setState((state: any) => Object.assign({}, state, {
                configurations,
                user_email: email
            }))
            console.log(this.state)
        })
    }

    render() {
        const { user_email, configuration_name, configurations } = this.state
        const { handleModalToggle, index } = this.props
        return (
            <div>
                <Filter 
                    user_email={user_email} 
                    configuration_name={configuration_name}
                    configurations={configurations}
                    handleModalToggle={handleModalToggle}
                    index = {index}
                />
            </div>
        )
    }
}

export default FilterContainer