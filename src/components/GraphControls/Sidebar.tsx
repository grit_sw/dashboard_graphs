import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles, useTheme, Theme, createStyles } from '@material-ui/core/styles';
import { Link } from "react-router-dom";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { connect } from 'react-redux';
import { getConfig } from '../../selectors/global';
import { getUserDetails } from '../../selectors/user';
import { getError } from '../../selectors/error';
import { doChangeAuth } from '../../actions/auth';
import store from '../../store';
import { doFetchConfig } from '../../actions/global';
import { doSaveUserDetails } from '../../actions/user';
import { demoConfig } from '../../api/config';
import userConfig  from '../../api/config';
import { flexbox } from '@material-ui/system';

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    drawer: {
      [theme.breakpoints.up('sm')]: {
        width: drawerWidth,
        flexShrink: 0,
      },
    },
    appBar: {
      marginLeft: drawerWidth,
      [theme.breakpoints.up('sm')]: {
        width: `calc(100% - ${drawerWidth}px)`,
      },
    },
    menuButton: {
      marginRight: theme.spacing(2),
      [theme.breakpoints.up('sm')]: {
        display: 'none',
      },
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
    },
    dialog: {
        width: drawerWidth * 2,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
    icon: {
        color: 'black'
    }
  }),
);

interface ISidebarProps {
    SidebarRoutes: Array<{ name: string, path: string, component: any, iconComp: any, open: boolean }>;
    config: any;
    user: any;
    error: String;
    onUpdateAuth: Function;
    onFetchConfig: Function;
    onFetchUser: Function;
}

const ResponsiveDrawer = function (props: ISidebarProps) {
    const { SidebarRoutes } = props;
    const classes = useStyles();
    const theme = useTheme();
    const [mobileOpen, setMobileOpen] = React.useState(false);
    const [modals, setModalOpen] = React.useState(SidebarRoutes);
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
    
    console.log(store.getState().errorState)
    function handleDrawerToggle() {
        setMobileOpen(!mobileOpen);
    }
    
    function handleModalToggle(index: number) {
        const newModals = modals.map((item, itemIndex) => {
            if (index === itemIndex) {
                // console.log(index, itemIndex)
                // console.log(item.open)
                item.open = !item.open
                return item;
            }
            else {
                // console.log(newModals)
                // console.log(itemIndex)
                // console.log(index)
                return item;
            }
        });
        console.log(setModalOpen)
        setModalOpen(newModals)
    }
    
    function handleLogout() {
        console.log("logging out...");
        localStorage.removeItem("token");
        localStorage.removeItem("email");
        localStorage.removeItem("full_name");
        localStorage.setItem("email", "workshop@grit.systems");
        props.onUpdateAuth(false);
        console.log(store.getState().authState);
        props.onFetchConfig('demo');
        props.onFetchUser({
            name: 'Demo'
        })
        location.reload()
    }

    const drawer = (
    <div>
        <div className={classes.toolbar} style={{display: "flex", alignItems: "center", justifyContent: "center"}}>
        <Typography variant="h6" >
            {props.user.name}
        </Typography>
        </div>
        <Divider />
        <List>
        {SidebarRoutes.map((item, index) => (
            <React.Fragment key={index}>
                <ListItem button onClick={item.name === 'Logout' ?  () => handleLogout() : () => {console.log(item.open); return handleModalToggle(index)}}>
                    <ListItemIcon><item.iconComp className={classes.icon}/></ListItemIcon>
                    <ListItemText
                        primary={<Typography variant="subtitle1" className={classes.icon}>{item.name}</Typography>}
                    />
                </ListItem>
                <Dialog
                    fullScreen={fullScreen}
                    open={item.open}
                    onClose={() => handleModalToggle(index)}
                    classes={{
                        paper: classes.dialog,
                    }}
                >
                    <item.component index={index} handleModalToggle={handleModalToggle}/>
                </Dialog>
            </React.Fragment>
            ))}
        </List>
    </div>
    );

    return (
    <div className={classes.root}>
        <CssBaseline />
        <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
            <IconButton
            color="inherit"
            aria-label="Open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
            >
            <MenuIcon />
            </IconButton>
            <Typography variant="h6" noWrap>
                {props.config.name}
            </Typography>
        </Toolbar>
        <Toolbar style={{paddingTop: 0, minHeight: 16, marginBottom: 10}}>
            <div>
                <Typography style={{color: "red", fontSize: 20, display: "block"}} variant="h3" noWrap>
                    {props.error}
                </Typography>
            </div>
        </Toolbar>
        </AppBar>
        <nav className={classes.drawer} aria-label="Mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
            <Drawer
                // container={container}
                variant="temporary"
                anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                open={mobileOpen}
                onClose={handleDrawerToggle}
                classes={{
                    paper: classes.drawerPaper,
                }}
                ModalProps={{
                    keepMounted: true, // Better open performance on mobile.
                }}
            >
            {drawer}
            </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
            <Drawer
            classes={{
                paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
            >
            {drawer}
            </Drawer>
        </Hidden>
        </nav>
        <main className={classes.content}>
        <div className={classes.toolbar} />
        </main>
    </div>
    );
}

const mapStateToProps = (state : any) => ({
    config: getConfig(state),
    user: getUserDetails(state),
    error: getError(state)
})

const mapDispatchToProps = (dispatch : any) => ({
    onUpdateAuth: (auth : boolean) => {
        dispatch(doChangeAuth(auth))
    },
    onFetchConfig: (type : string)=> {
        dispatch(doFetchConfig(type))
    },
    onFetchUser: (userObj: any) => {
        dispatch(doSaveUserDetails(userObj))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(ResponsiveDrawer)