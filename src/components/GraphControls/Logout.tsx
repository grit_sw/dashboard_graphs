import React from 'react'

class Logout extends React.Component {
    handleClick() {
        console.log("logged out")
    }

    render() {
        return (
            <div onClick={this.handleClick}></div>
        )
    }
}

export default Logout