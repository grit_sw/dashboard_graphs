import React, { Component } from 'react';
import { ExportContainerState } from './ExportForm/ExportInterface';
import { ExportContainerProps } from './ExportForm/ExportInterface';
import Export from './Export';
import Spinner from '../../components/Spinner';



class ExportContainer extends Component<ExportContainerProps, ExportContainerState>{
    constructor(props: any) {
        super(props);
        this.state = {
            display: 'form',
            data: []
        }
    }

    handleDisplay = (type: string) => {
        this.setState({
            display: type
        })
    }

    handleData = (data: Array<Object>) => {
        this.setState({
            data: data
        })
    }

    render() {
        return (
            <Export 
            display={this.state.display} 
            handleDisplay={this.handleDisplay} 
            handleData={this.handleData}
            handleModalToggle={this.props.handleModalToggle}
            data={this.state.data}
            />
        )
 
                    // if(this.state.display === 'form') {
                    //     console.log("Displaying form")
                    //     return (
                    //         <Export handleDisplay={this.handleDisplay} handleData={this.handleData}/>
                    //     )
                    // } else if( this.state.display === 'data') {
                    //     console.log("Displaying link")
                    //     return (
                    //         <CSVLink data={this.state.data}>Download me</CSVLink>
                    //     )
                    // } else if (this.state.display === 'loading') {
                    //     console.log("Displaying Spinner")
                    //     return (
                    //         <Spinner />
                    //     )
                    // }

                    // this.state.display === 'form' ?
                    // (<Export index={this.props.index} 
                    //     handleModalToggle={this.props.handleModalToggle} 
                    //     handleDisplay={this.handleDisplay}
                    //      handleData={this.handleData}
                    // />) :
                    // ( <CSVLink data={this.state.data}>Download me</CSVLink> )
    }
}


export default ExportContainer;