import React, {Component} from 'react';
import color from '../colors/colors';
import {RadialChart, Hint} from 'react-vis';

interface IDonutProps {
	Data: any;
}

interface IDonutValues {
	value: Boolean | any,
}


class Donut extends Component<IDonutProps, IDonutValues> {
	constructor(props: IDonutProps) {
		super(props)
		this.state = {
			value: false,
		}
	}
	render() {
		const { value } = this.state;
		const { Data } = this.props;
		return (
			<RadialChart
				className={'donut-chart-example'}
                colorRange={[color.generator, color.inverter, color.grid]}
				innerRadius={75}
				radius={140}
				getAngle={(d: any )=> d.angle}
				data={Data}
				// onValueMouseOver={(v: any) => this.setState({value: v})}
				// onSeriesMouseOut={(v: any) => this.setState({value: false})}
				width={300}
				height={300}
                padAngle={0.04}
                // showLabels
			>
				{value !== false && <Hint value={value} />}
				{/* {value !== false && console.log(value) } */}
			</RadialChart>
		);
	}
}
export default Donut;
