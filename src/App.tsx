import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import './App.css';
import Home from './views/Home';
class App extends Component {
  render() {
    return (
      <div className="App">
        <BrowserRouter>
            <Switch>
                <Route path="/" name="Home" component={Home} />
            </Switch>
        </BrowserRouter>

        {/* <Controls />
        <Graphs /> */}
      </div>
    );
  }
}

export default App;
