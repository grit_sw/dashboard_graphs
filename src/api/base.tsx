const esUrl = "https://es.grit.systems";
export const Services = "https://dev-backend.gtg.grit.systems";
// export const Services = "http://dev-backend.gtg.grit.systems:5500";
// const esUrl = "http://localhost:3000";
// if (process.env.NODE_ENV !== 'production') {
//   const esUrl = "https://es.grit.systems";;
// }

interface LooseObject {
  [key: string]: any
}


export const axiosConfig : LooseObject = {
  headers: { 
    "Access-Control-Allow-Origin": "*",
  },
  // maxContentLength: 10 * 1024 * 1024,
};

export const elasticSearchConfig : LooseObject = {
  headers: {
    "Access-Control-Allow-Origin": "*",
    "X-API-KEY": localStorage.getItem("token")
  }
}

export default esUrl;
