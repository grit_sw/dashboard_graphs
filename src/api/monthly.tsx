import Axios from "axios";
import MyBaseUrl, { axiosConfig } from './base';

const GetMonthly = (configId: string, timeStart: string, timeStop: string) => {
    return Axios.get(`${MyBaseUrl}/monthly/${configId}/${timeStart}/${timeStop}`, axiosConfig);
};

export default GetMonthly