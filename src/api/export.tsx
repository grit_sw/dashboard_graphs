import Axios from "axios";
import esUrl, { axiosConfig } from './base';

const ExportRequest = (configId: string, exportStartTime: string, exportStopTime: string, interval: string, intervalUnit:string) => {
    return Axios.get(`${esUrl}/exportToCsv/${configId}/${exportStartTime}/${exportStopTime}/${interval}/${intervalUnit}`, axiosConfig);
};

export default ExportRequest
