import Axios from "axios";
import MyBaseUrl, { axiosConfig } from './base';

const GetRecent = (configId: string, timeStart: string, secondsInterval: string) => {
    return Axios.get(`${MyBaseUrl}/recent/${configId}/${timeStart}/${secondsInterval}`, axiosConfig);
};

export default GetRecent