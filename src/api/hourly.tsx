import Axios from "axios";
import MyBaseUrl, { axiosConfig } from './base';

const GetHourly = (configId: string, timeStart: string, timeStop: string) => {
    return Axios.get(`${MyBaseUrl}/daily/${configId}/${timeStart}/${timeStop}`, axiosConfig);
};

export default GetHourly