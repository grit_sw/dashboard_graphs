import Axios from "axios";
import MyBaseUrl, { axiosConfig } from './base';

const GetData = (configId: string, timeStart: string, timeStop: string) => {
    return Axios.get(`${MyBaseUrl}/data/${configId}/${timeStart}/${timeStop}`, axiosConfig);
};

export default GetData