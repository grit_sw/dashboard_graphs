import Axios from "axios";
import { Services, axiosConfig } from './base';

const ExportRequest = (body: any) => {
    return Axios.post(`${Services}/auth/login/`, body, axiosConfig);
};

export default ExportRequest